/**
 * @file 	KernelInterface.cpp
 * @details This file implements common parts of the KernelInterface-Class
 * @date 	25.11.2017
 * @author 	Christian Jütte, Silvan König
 * @version	2.0
 *
 * @remarks Good info at:
 * 			- https://www.freebsd.org/cgi/man.cgi?query=ntp_adjtime&sektion=2
 * 			- http://www.gnu.org/software/libc/manual/html_node/High-Accuracy-Clock.html
 */

#include "KernelInterface.hpp"
#include "../Log/Logger.hpp"
#include "../NTP.hpp" // included here because of declaration conflicts

#include <chrono>
#include <iostream>
#include <thread>

#include <cstdlib>

#if defined(NTP_USE_BOOST)

#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>

#endif // defined(NTP_USE_BOOST

#if defined(NTP_WINDOWS)
#include "KernelInterface_WIN32.inc"
#endif

#if defined(NTP_LINUX)
#include "KernelInterface_Linux.inc"
#endif

#if defined(NTP_USE_BOOST)
#include "KernelInterface_boost.inc"
#endif

namespace ntp
{

#if defined(NTP_USE_BOOST)
using namespace boost::interprocess;
#endif

/**
 *
 * @brief 	Constructor
 *
 * @details Loads systemPrecision from Constants using the key "SYSTEM_PRECISION".
 *          It also initializes the the shared memory, the semaphore and the networking
 *          system.
 *
 *          The implementation differs depending on the configuration (which platform / using boost or not).
 *
 */
#if defined(NTP_WINDOWS)

KernelInterface::KernelInterface() : ioservice{},
                                     // under Windows we can't specify the size later ('truncate'), so we do it here.
                                     shm{open_or_create, SHM_NAME, read_write, OFFSET_SYSTEMVARIABLES + sizeof(systemVariables)},
                                     pfll{},
                                     sharedSystemVariablesRegion{shm, read_write, OFFSET_SYSTEMVARIABLES, sizeof(systemVariables)},
                                     sharedServerProcessId{shm, read_write, OFFSET_SERVER_PID, PID_SIZE},
                                     sharedClientProcessId{shm, read_write, OFFSET_CLIENT_PID, PID_SIZE},
                                     shmSemaphore{open_or_create, SHM_NAME, 1}
{
	systemPrecision = ntp::Constants::getInstance().getChar(ntp::ConstantsChar::systemPrecision);
	startTime = std::chrono::steady_clock::now();
	this->udpsocket = nullptr;
	this->leapSecond = 0;
	sharedMemInitialized = true;

	DEBUG("KernelInterface initialized.");
}

#endif // defined(NTP_WINDOWS)

#if defined(NTP_LINUX)

KernelInterface::KernelInterface()

#if defined(NTP_USE_BOOST)

    : ioservice{},
      shm{open_or_create, SHM_NAME, read_write},
      sharedSystemVariablesRegion{shm, read_write, OFFSET_SYSTEMVARIABLES, sizeof(systemVariables)},
      sharedServerProcessId{shm, read_write, OFFSET_SERVER_PID, PID_SIZE},
      sharedClientProcessId{shm, read_write, OFFSET_CLIENT_PID, PID_SIZE},
      shmSemaphore
{
	open_or_create, SHM_NAME, 1
}
#endif
{
	systemPrecision = ntp::Constants::getInstance().getChar(ntp::ConstantsChar::systemPrecision);
	sockfd = -1;
	startTime = std::chrono::steady_clock::now();

#if defined(NTP_USE_BOOST)
	// this is crazy. The constructor which includes the truncate operation can be used within Windows,
	// but does not work under Linux. However, we truncate here.
	shm.truncate(OFFSET_SYSTEMVARIABLES + sizeof(systemVariables));
	this->udpsocket = nullptr;
	sharedMemInitialized = true;
#endif

	DEBUG("KernelInterface initialized.");
} // KernelInterface::KernelInterface()

#endif // defined(NTP_LINUX)

/**
 *
 * @brief 	Private Standard-Destructor. Closes the socket and deletes the PID from shared memory.
 *
 */
KernelInterface::~KernelInterface()
{
	DEBUG("Cleaning up... ");

	if (socketInitialized)
	{
		closeSocket();
	}

	if (sharedMemInitialized)
	{
		//Delete our pid from the shared memory object.
		//We leave the reference timestamp there, since this is actually the last time we synced!
		DEBUG("Clearing PID from shared memory.");
		deletePIDFromSharedMem();
	}

	DEBUG("Cleanup done.");
}

/**
 *
 * @brief	Returns the Singleton-Object of the class.
 *
 * @details It just returns the internal object.
 */
ntp::KernelInterface& KernelInterface::getInstance()
{
	return instance;
}

/**
 *
 * @brief	Function to get the current process-Time,
 * 			that is the time the program has been running.
 *
 * @details	This can be used to compare times of packet arrival
 * 			or last updates, when the system time has been
 * 			adjusted.
 *
 * @returns	The current time the process has been running as
 * 			a std::chrono::time_point as specified in \<chrono>.
 *
 */
std::chrono::steady_clock::time_point KernelInterface::getProcessTime()
{
	std::chrono::steady_clock::time_point time = std::chrono::steady_clock::now();
	return time;
}

/**
 *
 * @brief	Function to get the current process-Time,
 * 			that is the time the program has been running, in miliseconds
 *
 * @details	This should only be used for logging purposes because of the limited resolution
 *
 * @returns	The current time the process has been running in miliseconds
 *
 */
unsigned long KernelInterface::getProcessTimeMS()
{
	double time = processTimeToDouble(getProcessTime());
	return static_cast<unsigned long>(std::abs(time) * 1000 + 0.5);
}

/**
 * @brief	Calculates the difference in time between two std::chrono::time_points.
 * 			The calculation performed is tp1-tp2.
 *
 * @return	A double (signed!) that contains the difference between the time_points in seconds.
 *
 */
double KernelInterface::calcTimeDifference(std::chrono::steady_clock::time_point tp1, std::chrono::steady_clock::time_point tp2)
{
	return (std::chrono::duration_cast<std::chrono::duration<double>>(tp1 - tp2)).count();
}

/** @brief	When a processTime-Timestamp is supplied, the function converts it to a double
 * 			that represents the number of seconds since the start of the program.
 *
 *
 */
double KernelInterface::processTimeToDouble(std::chrono::steady_clock::time_point tp)
{
	return calcTimeDifference(tp, startTime);
}

#if _MAKE_DOCUMENTATION

/**
 * @brief	Tries to lock the semaphore for accessing shared memory. This may also create the semaphore.
 * @details	When compiled with boost, this method will be undefined. The class ScopedSemaphore is the better way
 * 			to do the job.
 *
 * @returns	Returns true if the semaphore was successfully locked.
 * 			If false is returned, this means that something irregular has happened and the shared memory should not
 * 			be accessed.
 */
bool KernelInterface::lockSemaphore()
{
}

/**
 *
 * @brief 	Tries to unlock and free the semaphore so that other processes can obtain it.
 * @details	When compiled with boost, this method will be undefined. The class ScopedSemaphore is the better way
 * 			to do the job.
 *
 *
 * @returns Returns true on success and false on failure.
 *
 */
bool KernelInterface::freeSemaphore()
{
}

/**
 * @brief	Utility function for checking if two processes with different PIDs have the same name.
 * @details	Under Linux the function uses the file /proc/$PID/comm to get the name.
 *
 * 			Under Windows this method uses the WIN32-API-function GetProcessImageFileName().
 *
 * @return	Returns true, if the processes do have the same name, and false if they don't.
 * 			If one or both processes are not in the list, false is returned.
 *
 */
bool KernelInterface::doProcessesHaveSameName(pid_t processOne, pid_t processTwo)
{
}

/**
 * @brief	This method creates a Unix timestamp from an NTP timestamp.
 * @details	Creating a Unix timestamp from an NTP timestamp means subtracting 70 years
 * 			and converting the timestamp to timeval.
 *
 *			If the NTP timestamp is less than UNIX_NTP_TIME_OFFSET (so it refers to a date before 1970),
 *			the timestamp will be interpreteted as one of the next NTP-era (starts in 2036).
 *			Therefore UINT32_MAX - UNIX_NTP_TIME_OFFSET will be added.
 *
 *			Since this method is Linux-specific, it will not be included when compiled for Windows.
 *
 * @bug		In the future (when Linux timekeeping has changed), the current era should also
 * 			be considered in this.
 *
 * @see		UNIX_NTP_TIME_OFFSET, Timestamp(const timeval &time)
 */
timeval KernelInterface::ntpToUnixTimestamp(const Timestamp& tmstmp) const
{
}

/**
 * @brief	Constructs a NTP-Timestamp from the Unix timestamp format.
 * @details	When using this function, the Unix timestamp will be converted to
 * 			NTP format. This means adding 70 years to the Unix Timestamp.
 *
 *			Since this method is Linux-specific, it will not be included when compiled for Windows.
 *
 * @see		getUnixTimestamp, UNIX_NTP_TIME_OFFSET
 */
LongTimestamp KernelInterface::unixToNTPTimestamp(const timeval& time) const
{
}

/**
 *
 * @brief	Gets the current system time.
 *
 * @returns An object of type ntp::LongTimestamp, containing
 * 			the current system time.
 *
 * @throws ntp::TimeException	Throws a ntp::TimeException if the time could not be fetched
 *
 * @remarks Note that this function may need system specific rights. See programHasNecessaryRights() for details.
 */

ntp::LongTimestamp KernelInterface::getTime()
{
}

/**
 *
 * @brief	Adjusts the system time according to the provided offset.
 *
 * @details Under Linux, the function uses the ntp-clock-adjustment-algorithms
 * 			implemented in <sys/timex.h>. RFC 1589 provides more information on the usage.
 *
 * 			Under Windows, those algorithms are provided by the class PhaseFrequencyLockLoop.
 *
 * @param [in] offset		The offset in seconds of the system time as computed by the
 * 							Peer- and System-process.
 *
 * @remarks Note that this function may need system specific rights. See programHasNecessaryRights() for details.
 *
 * @throws	ntp::TimeException	If the time could not be adjusted.
 *
 */

void KernelInterface::adjustTime(double offset)
{
}

/**
 *
 * @brief	Sets the frequency offset of the system clock. Used in the clock-adjust-algorithm.
 *
 * @details The function uses the ntp-clock-adjustment-algorithms
 * 			implemented in <sys/timex.h>. RFC 1589 provides more information on the usage.
 *
 * 			Under Windows, those algorithms are provided by the class PhaseFrequencyLockLoop.
 *
 * @param [in] frequencyOffset	The frequency offset of the system clock. This
 * 		  					is given in the unit microseconds/second (PPM).
 *
 * @remarks Note that this function may need system specific rights. See programHasNecessaryRights() for details.
 *
 * @throws	ntp::TimeException	If the frequency offset could not be set.
 *
 */

void KernelInterface::setFrequencyOffset(double frequencyOffset)
{
}

/**
 * @brief	Gets the current frequency offset and returns it.
 *
 * @return	Returns the current frequency offset in PPM (e.g. us/s).
 *
 * @remarks Note that this function may need system specific rights. See programHasNecessaryRights() for details.
 *
 * @throws	ntp::TimeException	If the current status could not be obtained.
 *
 */
double KernelInterface::getFrequencyOffset()
{
}

/**
 *
 * @brief	Sets the system time according to the provided offset.
 *
 * @details	Under Linux this function uses the settimeofday-function from <sys/time.h>.
 * 			Under Windows, it uses the SetSystemTime-function.
 *
 * @param [in] offset	The offset of the system time (in seconds) as computed by the
 * 						Peer- and System-process.
 *
 * @remarks Note that this function may need system specific rights. See programHasNecessaryRights() for details.
 *
 * @throws	ntp::TimeException	If the time could not be set.
 *
 */

void KernelInterface::setTime(double offset)
{
}

/**
 * @brief	Returns the precision of the system in log2-seconds.
 * 			Under Linux the value is always updated when adjustTime is called.
 * 			Under Windows it is constant.
 *
 */

signed char KernelInterface::getSystemPrecision()
{
}

/**
 * @brief Returns the clock-jitter in seconds
 *
 * @throws	ntp::TimeException	If the jitter could not be fetched
 */

double KernelInterface::getJitter()
{
}

/**
 * @brief	Returns the currently set offset of the clock in seconds.
 *
 * @throws	ntp::TimeException	If the offset could not be fetched
 */
double KernelInterface::getOffset()
{
}

/**
 * @brief	Inserts a leap second at the end of the day (clock jumps from 23:59:59 to 23:59:60 and then to 00:00:00)
 *
 * @details Uses ntp_adjtime under Linux. This is not supported under Windows.
 *
 * @throws	ntp::TimeException	Throws a ntp::TimeException if the leap second could not be inserted.
 */
void KernelInterface::insertLeapSecond()
{
}

/**
 * @brief	Deletes a leap second at the end of the day (clock jumps from 23:59:58 to 00:00:00)
 *
 * @details Uses ntp_adjtime. This is not supported under Windows.
 *
 * @throws	ntp::TimeException	Throws a ntp::TimeException if the leap second could not be deleted.
 */
void KernelInterface::deleteLeapSecond()
{
}

/**
 * @brief	Resets both leap second flags (no second will be inserted or deleted at the end of the day)
 *
 * @details Uses ntp_adjtime. Call this every time the program starts!
 *  		This is not supported under Windows.
 *
 * @throws	ntp::TimeException	Throws a ntp::TimeException if the leap second could not be reset.
 */
void KernelInterface::resetLeapSecond()
{
}

/**
 * @brief	Returns the current status of the leap second. For return values please see below.
 * @details	This is not supported under Windows.
 *
 * @returns	Returns an unsigned char carrying information about the status of the leap seconds. Values are
 * 			the following:
 * 			Return Value			|	Meaning
 * 			----------------------- |	-------------------------------
 * 			LI_NO_WARNING			|	No leap second is inserted or deleted at the end of the day.
 * 			LI_61SEC				|	A leap second will be inserted at the end of the day. The clock will go from 23:59:59 to 23:59:60 and then to 00:00:00.
 * 			LI_59SEC				| 	A leap second will be deleted at the end of the day. The clock will go from	23:59:58 directly to 00:00:00.
 * 			LI_UNKNOWN				|	Some error has happened / the clock is unsynchronized.
 *
 *	@throws	ntp::TimeException	Throws a ntp::TimeException if the value could not be obtained..
 *
 *	@note	This was changed from using ntp_gettime to using shared system variables since
 *			there were problems in which STA_UNSYNC was always reset so 1.
 */
ntp::LeapIndicator KernelInterface::getLeapSecond()
{
}

/**
 * @brief 	Helper function for non-blocking console I/O.
 * 			Checks if input is available on the console (i.e., the user has typed and pressed return).
 * 			Will return true if something can be read using std::getline(std::cin, ...).
 *
 * @details	Under Windows, this method uses the PeekConsoleInput()-function supplied by the WIN32-API.
 * 			The Linux-approach is explained here: http://cc.byexamples.com/2007/04/08/non-blocking-user-input-in-loop-without-ncurses/
 *
 */

bool KernelInterface::consoleInputAvailable()
{
}

/**
 * @brief	Checks if the program has all necessary rights to run
 * @details	This method will check if the program has every right necessary to run.
 * 			This method is located in the KernelInterface, because rights management
 * 			is a topic related to the operating system.
 *
 * 			This program needs rights to:
 * 				- read/set the system clock
 * 				- acquire, lock and write to shared memory (used for communication between local client and server)
 * 				- bind to UDP/IP-Socket (either for listening on port 123 or sending packets)
 * 				- doing file-IO (for logging purposes)
 *
 * 			As of Linux, all operations mentioned can be done if the program runs as root.
 * 			So, currently this method will check if the program runs with the effective UID 0 (root).
 *
 * 			Under Windows, this method will check the privileges, the user executing the program has.
 * 			If the Privileges are not enabled, the function tries to enable them.
 *
 * @remarks	It might be better to use the Linux capability interface for checking for individual rights
 * 			and even obtaining them.
 */
bool KernelInterface::programHasNecessaryRights()
{
}

/**
 * @brief	Wrapper for function htonl.
 * @details	Changes byte order from host byte order to network byte order.
 *
 */
std::uint32_t KernelInterface::hostToNetworkOrder(std::uint32_t figure)
{
}

/**
 * @brief	Wrapper for function htons.
 * @details	Changes byte order from host byte order to network byte order.
 */
std::uint16_t KernelInterface::hostToNetworkOrder(std::uint16_t figure)
{
}

/**
 * @brief	Resets the PID of this implementation in the shared memory object to 0, so that another process
 * 			can be started.
 *
 */
void KernelInterface::deletePIDFromSharedMem()
{
}

/**
 * @brief	Checks if no another process of the same kind (client/server) is already running and initializes shared memory
 *
 * @details	For this, the shared memory is used. First 64bit contain the pid of the client
 * 			(in binary, not ascii), next 64 bits the pid of the server, next the sharedSystemVariables
 *
 * 			If a pid is in the file, it doesn't match the current processes pid, and it is named the same as the current
 * 			process, another process of NTP is running.
 *
 * 			This function also updates the file accordingly if it can.
 *
 * 	@return	Returns true if another process of the same kind is running, and false if not.
 *
 * 	@exception ntp::FileException	Throws an error if the shared memory object could not be created in
 * 									Client-Mode or the access failed.
 */

bool KernelInterface::isOtherProcessRunning()
{
}

/**
 * @brief	Initializes the socket depending on the mode the program runs in.
 * @details	Before calling this method, make sure the class NTP has been initialized and
 * 			NTP::getMode() returns a valid value.
 * 			The socket is initialized with a timeout for receive-operations so we can
 * 			end the Receiver-Thread properly in case of an exit.
 * @exception ntp::NetworkException 	Throws a fatal error if the socket could not be created or the port
 * 										could not be bound.
 */
void KernelInterface::initSocket()
{
}

/**
 * @brief	closes the socket
 */
void KernelInterface::closeSocket()
{
}

/**
 * @brief	Converts a type ntp::IPAddress to the type sockaddr_in
 * @details Since this method is Linux-specific, it will not be included when compiled for Windows.
 *
 */

sockaddr_in KernelInterface::ntpToUnixIPAddress(const IPAddress& address) const
{
}

/**
 * @brief	Converts a type sockaddr_in to the type ntp::IPAddress
 * @details	Since this method is Linux-specific, it will not be included when compiled for Windows.
 *
 */

IPAddress KernelInterface::unixToNTPIPAddress(const sockaddr_in& addr) const
{
}

/**
 *
 * @brief	Transmits the ntp::NTPPacket of the message provided to the ip-address of the message.
 *
 * @param [in] msg The ntp::Message-object to be transmitted.
 *
 * @throws 	ntp::NetworkException	Throws a ntp::NetworkException when the message could not be transmitted.
 *
 */

void KernelInterface::transmitPacket(ntp::Message& msg)
{
}

/**
 *
 * @brief 	Function to receive Packets. To be called by the receiver. Blocking function.
 *
 * @details Waits until a valid UDP-Packet was received on the Port. If a packet
 * 			has been received, it tries to create a ntp::Message.
 *
 * 			If a ntp::Message has been created successfully, the message is returned.
 *
 * 			The function will block until a specified timeout. If no packet is received
 * 			until the timeout, a ntp::TimeoutException is thrown.
 *
 * @returns Returns a shared_ptr containing a pointer to the received Message.
 * 			The Message will be deleted once all instances of the shared_ptr are deleted.
 *
 * @throws	Throws an ntp::TimeoutException if the function times out.
 * @trows	Throws an ntp::InvalidPacketException if a malformed packet was received.
 */
std::shared_ptr<ntp::Message> KernelInterface::receivePacket()
{
}

/**
 * @brief	Gets the corresponding IPv4 address to a hostname.
 *
 * @details Uses the function getaddrinfo when compiling for native Linux.
 *
 *			When compiling with boost, the udp::resolver will be used.
 *
 * @param [in] hostname String containing the host-address.
 *
 * @throws 	ntp::NetworkException	Throws a ntp::NetworkException if the name could not be resolved.
 */
IPAddress KernelInterface::getAddressFromName(std::string hostname)
{
}

/**
 * @brief 	Function for the Client to let the server know some system variables
 *
 * @details Since this application only runs as either server or client, using both server and
 * 			client needs to be done by running two differently configured processes in parallel.
 *
 * 			In order to have the server know some system variables, they are put into shared memory
 * 			by the client. The server can then read them out.
 *
 * 			Access to shared memory is protected by a semaphore.
 *
 * 			The function is also used to initialize the system variables at start of execution.
 *
 * @param 	newSystemVariables [in] The variables-object to be stored into shared memory.
 *
 * @return	The function returns true if the variables were successfully put into shared memory,
 * 			and false if an error happened.
 *
 */
bool KernelInterface::updateSystemVariables(SharedSystemVariables newSystemVariables)
{
}

/**
 * @brief	This function tries to open and read out a shared memory object containing the shared system variables.
 * 			If no object can be opened, or no data can be read, it will return a default object.
 *
 * @returns On success, the object that was stored in the shared memory. On failure, a default object.
 */
SharedSystemVariables KernelInterface::getSystemVariables()
{
}

/**
 * @brief	Checks if no another process of the same kind (client/server) is already running and initializes shared memory
 *
 * @details	For this, the shared memory is used. First 64bit contain the pid of the client
 * 			(in binary, not ascii), next 64 bits the pid of the server, next the sharedSystemVariables
 *
 * 			If a pid is in the file, it doesn't match the current processes pid, and it is named the same as the current
 * 			process, another process of NTP is running.
 *
 * 			This function also updates the file accordingly if it can.
 *
 * 	@return	Returns true if another process of the same kind is running, and false if not.
 *
 * 	@exception ntp::FileException	Throws an error if the shared memory object could not be created in
 * 									Client-Mode.
 */
bool KernelInterface::isOtherProcessRunning()
{
}

#endif

} //End of namespace ntp
