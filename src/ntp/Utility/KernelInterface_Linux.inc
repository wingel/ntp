/*
* ----------------------------------------------------------------------------
* Copyright 2018 Ostfalia University of Applied Sciences, Germany
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* ----------------------------------------------------------------------------
*/

/**
 * @file 	KernelInterface_Linux.cpp
 * @details This file implements the parts of the KernelInterface-Class, that are used when compiling for native Linux.
 * @date 	05.06.2016
 * @author 	Christian Jütte, Silvan König
 * @version	2.0
 *
 * @remarks Good info at:
 * 			- https://www.freebsd.org/cgi/man.cgi?query=ntp_adjtime&sektion=2
 * 			- http://www.gnu.org/software/libc/manual/html_node/High-Accuracy-Clock.html
 */

#include <algorithm> //remove_if
#include <arpa/inet.h>
#include <chrono>
#include <cmath>   //log2
#include <cstring> //std::strerror
#include <errno.h> //errno
#include <memory>  //shared_ptr
#include <netdb.h> //addrinfo
#include <netinet/in.h>
#include <sstream>
#include <stdio.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>  //settimeofday
#include <sys/timex.h> //NTP-Functions
#include <unistd.h>    //close, getpid, ftruncate
#include <vector>

#include <future>

// Shared Memory
#include <fcntl.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <sys/stat.h> /* For mode constants */

#include <cstdint>

#if defined(NTP_USE_BOOST)

#include "ScopedSemaphore.hpp"

#endif

#ifdef NTP_MUSL_SUPPORT

#define ntp_adjtime adjtimex

extern "C" int ntp_gettime(struct ntptimeval* ntv)
{
	struct timex tx
	{
	};

	int result = ntp_adjtime(&tx);

	ntv->time = tx.time;
	ntv->maxerror = tx.maxerror;
	ntv->esterror = tx.esterror;

	return result;
}

#endif

#define HIER DEBUG("HIER");

namespace ntp
{

#if defined(NTP_LINUX)

bool KernelInterface::lockSemaphore()
{
	bool success = true;
	std::string error = "";

	//Open the semaphore
	sem_t* lock = sem_open(SHM_NAME, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH, 1);
	if (lock == SEM_FAILED)
	{
		int tempErrno = errno;
		error = "Couldn't open or create the semaphore: ";
		error.append(strerror(tempErrno));
		success = false;
	}
	else
	{
		//DEBUG("Semaphore opened");
		//Wait for locking
		if (sem_wait(lock) < 0)
		{
			int tempErrno = errno;
			error = "Couldn't wait for the semaphore: ";
			error.append(strerror(tempErrno));
			success = false;
		}

		//Close the semaphore
		if (sem_close(lock) < 0)
		{
			int tempErrno = errno;
			error = "Couldn't close the semaphore: ";
			error.append(strerror(tempErrno));
		}
	}

	if (!success && NTP::getMode() != NTP::AppMode::MODE_SERVER)
	{
		WARN(error);
	}

	return success;
}

bool KernelInterface::freeSemaphore()
{
	bool success = true;
	std::string error = "";

	//Open the semaphore
	sem_t* lock = sem_open(SHM_NAME, O_RDWR);
	if (lock == SEM_FAILED)
	{
		int tempErrno = errno;
		error = "Couldn't open the semaphore: ";
		error.append(strerror(tempErrno));
		success = false;
	}
	else
	{
		if (sem_post(lock) < 0)
		{
			int tempErrno = errno;
			error = "Couldn't free the semaphore: ";
			error.append(strerror(tempErrno));
			success = false;
		}

		//Close the semaphore
		if (sem_close(lock) < 0)
		{
			int tempErrno = errno;
			error = "Couldn't close the semaphore: ";
			error.append(strerror(tempErrno));
		}
	}

	if (!success && NTP::getMode() != NTP::MODE_SERVER)
	{
		WARN(error);
	}

	return success;
}

bool KernelInterface::doProcessesHaveSameName(pid_t processOne, pid_t processTwo)
{
	bool result = false;
	std::string nameOne = "";
	std::string nameTwo = "";

	std::string fileOne = "/proc/" + std::to_string(processOne) + "/comm";
	std::string fileTwo = "/proc/" + std::to_string(processTwo) + "/comm";

	std::ifstream input(fileOne);
	input >> nameOne;
	//DEBUG(nameOne);
	input.close();

	input.open(fileTwo);
	input >> nameTwo;
	//DEBUG(nameTwo);
	input.close();

	if (nameOne == nameTwo)
	{
		result = true;
	}

	return result;
}

timeval KernelInterface::ntpToUnixTimestamp(const Timestamp& tmstmp) const
{
	uint32_t unixtime = tmstmp.getSeconds();
	if (unixtime >= Timestamp::UNIX_NTP_TIME_OFFSET)
	{
		unixtime -= Timestamp::UNIX_NTP_TIME_OFFSET;
	}
	else
	{
		unixtime += UINT32_MAX - Timestamp::UNIX_NTP_TIME_OFFSET;
	}

	timeval t;
	time_t sec = unixtime;
	time_t usec = static_cast<time_t>(Timestamp::NTPLONG_FRAC_RESOLUTION * tmstmp.getFraction() * 1.0e6);

	t.tv_sec = static_cast<time_t>(sec);
	t.tv_usec = static_cast<suseconds_t>(usec);

	return t;
}

LongTimestamp KernelInterface::unixToNTPTimestamp(const timeval& time) const
{
	LongTimestamp tmstmp(static_cast<unsigned int>(time.tv_sec) + Timestamp::UNIX_NTP_TIME_OFFSET, static_cast<unsigned int>(1.0e-6 * time.tv_usec * UINT32_MAX));
	return tmstmp;
}

ntp::LongTimestamp KernelInterface::getTime()
{
	timex tmx{}; //Struct to set resolution to microseconds
	ntptimeval ntpTimeval;
	ntp::LongTimestamp tmstmp(0);

	//Set resolution of the system clock to microseconds
	tmx.modes = ADJ_MICRO;
	ntp_adjtime(&tmx);
	//Precision is returned in microseconds, must be converted to log2 seconds
	systemPrecision = static_cast<signed char>(log2(((double)tmx.precision) / 1000000));

	//Get the time
	if (ntp_gettime(&ntpTimeval) >= 0)
	{
		//Getting time successful, construct the Timestamp
		tmstmp = unixToNTPTimestamp(ntpTimeval.time);
		//std::cout << "getTime: s: " << ntpTimeval.time.tv_sec << " us: " << ntpTimeval.time.tv_usec << std::endl;
	}
	else
	{
		//Getting time unsuccessful
		std::stringstream errorString("Error in KernelInterface::getTime: The current system time could not be fetched: ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}

	return tmstmp;
}

void KernelInterface::adjustTime(double offset)
{
	struct timex tmx
	{
	}; //Time-struct

	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		std::stringstream errorString("Error obtaining the statusWord in KernelInterface::adjustTime(): ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}

	tmx.offset = offset * 1000000.0; //Set offset

	tmx.constant = ntp::Constants::getInstance().getUInt(ntp::ConstantsUInt::pllTimeConstant); //RFC 1589, p.29. Set to 0 as default for fast convergence

	tmx.modes = tmx.modes | ADJ_OFFSET | ADJ_MICRO | ADJ_TIMECONST; //Select resolution (microseconds)
	                                                                //and offset-adjustment
	tmx.modes = tmx.modes | ADJ_MAXERROR | ADJ_ESTERROR | ADJ_STATUS;

	tmx.status = (tmx.status | STA_PLL) & ~STA_UNSYNC; //Activate PLL, clock is synced

	tmx.esterror = static_cast<double>(systemVariables.getRootDispersion()) * 1e6;
	tmx.maxerror = static_cast<double>(systemVariables.getRootDispersion()) + static_cast<double>(systemVariables.getRootDelay()) / 2;

	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		std::stringstream errorString("Error in KernelInterface::adjustTime: The system time could not be adjusted: ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}

	//DEBUG("Status-Flag: " + std::to_string(tmx.status));
	//DEBUG("Modes: " + std::to_string(tmx.modes));
	//Precision is returned in microseconds, must be converted to log2 seconds
	systemPrecision = static_cast<signed char>(log2(((double)tmx.precision) / 1000000));

	//std::cout << "kInterfaceMaxerr: " << tmx.maxerror << " esterr: " << tmx.esterror << " [us]" <<std::endl;
}

void KernelInterface::setFrequencyOffset(double frequencyOffset)
{
	struct timex tmx
	{
	}; //Time-struct

	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		std::stringstream errorString("Error obtaining the statusWord in KernelInterface::adjustTime(): ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}

	tmx.modes = tmx.modes | ADJ_FREQUENCY | ADJ_MICRO; //Adjust Frequency

	int integerPart = static_cast<int>(frequencyOffset) << 16;
	unsigned short fractionalPart = static_cast<unsigned short>((UINT16_MAX * 1.0) * (frequencyOffset - integerPart));

	tmx.freq = (integerPart & 0xFFFF0000) + fractionalPart;

	//std::cout << "kInterface(calc) freqOffset (hex): " << std::hex << tmx.freq << std::endl;

	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting frequency unsuccessful, throw exception
		std::stringstream errorString("Error in KernelInterface::setFrequencyOffset: The frequency offset could not be set: ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}

	//std::cout << "kInterfacePPS freq: " << std::fixed << static_cast<double>(tmx.ppsfreq) / 65536.0 << " PPM" <<std::endl;
}

double KernelInterface::getFrequencyOffset()
{
	struct timex tmx
	{
	}; //Time-struct
	tmx.modes |= ADJ_MICRO;

	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		std::stringstream errorString("Error obtaining the frequencyOffset in KernelInterface::getFrequencyOffset(): ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}

	//Calculate frequency offset
	auto integerPart = static_cast<int>(tmx.freq >> 16);
	double frac = (tmx.freq & 0xFFFF) / 65536.0;

	return (integerPart * 1.0 + frac);
}

void KernelInterface::setTime(double offset)
{
	timeval tmVal{};
	tmVal = ntpToUnixTimestamp(getTime());

	// Using time_t here since the size of that might change when
	// the year 2038 problem gets fixed.

	int secs = static_cast<int>(trunc(offset));
	suseconds_t usecs = static_cast<suseconds_t>(trunc((std::abs(offset) - std::abs(secs)) * 1e6));

	usecs = usecs + tmVal.tv_usec;
	if (usecs < tmVal.tv_usec)
	{
		//Overflow occured
		if (secs >= 0)
		{
			secs++;
		}
		else
		{
			secs--;
		}

		usecs = usecs - 1e6;
	}

	tmVal.tv_sec = tmVal.tv_sec + secs;
	tmVal.tv_usec = usecs;

	if (settimeofday(&tmVal, NULL) < 0)
	{
		LOG_ERROR("Error in KernelInterface::setTime: The system time could not be set.");
		std::cout << "error number: " << errno << "   error string: " << std::strerror(errno) << " \n";
		std::cout << "--> secs (" << secs << "), usecs(" << usecs << "), tmVal.tv_sec(" << tmVal.tv_sec << "), tmVal.tv_usec(" << tmVal.tv_usec << ")\n";
	}
}

signed char KernelInterface::getSystemPrecision()
{
	return systemPrecision;
}

double KernelInterface::getJitter()
{
	struct timex tmx
	{
	}; //Time-struct
	tmx.modes |= ADJ_MICRO;

	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		std::stringstream errorString("Error obtaining the Jitter: ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}

	//Convert jitter from microseconds to double
	double jitter = (tmx.jitter * 1.0) / 1000000.0;
	return jitter;
}

double KernelInterface::getOffset()
{
	struct timex tmx
	{
	}; //Time-struct
	tmx.modes |= ADJ_MICRO;

	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		std::stringstream errorString("Error obtaining the Jitter: ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}

	//Convert offset from microseconds to double
	double offset = (tmx.offset * 1.0) / 1000000.0;
	return offset;
}

void KernelInterface::insertLeapSecond()
{
	struct timex tmx
	{
	}; //Time-struct

	tmx.modes = 0;
	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		std::stringstream errorString("Error obtaining the statusWord in KernelInterface::insertLeapSecond(): ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}

	tmx.modes = tmx.modes | MOD_STATUS;
	tmx.status = (tmx.status | STA_INS) & ~STA_DEL & ~STA_UNSYNC;
	//Insert leap second
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		std::stringstream errorString("Error inserting leap second in KernelInterface::insertLeapSecond(): ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}

	//Also set the leapIndicator in shared system variables
	SharedSystemVariables sysVar = getSystemVariables();
	sysVar.setLeapIndicator(ntp::LeapIndicator::LI_61SEC);
	updateSystemVariables(sysVar);

	DEBUG("Leap second inserted.");
}

void KernelInterface::deleteLeapSecond()
{
	struct timex tmx
	{
	}; //Time-struct

	tmx.modes = 0;
	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		std::stringstream errorString("Error obtaining the statusWord in KernelInterface::deleteLeapSecond(): ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}

	tmx.modes = tmx.modes | MOD_STATUS;
	tmx.status = (tmx.status | STA_DEL) & ~STA_INS & ~STA_UNSYNC;
	//Insert leap second
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		std::stringstream errorString("Error deleting leap second in KernelInterface::deleteLeapSecond(): ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}

	//Also set the leapIndicator in shared system variables
	SharedSystemVariables sysVar = getSystemVariables();
	sysVar.setLeapIndicator(ntp::LeapIndicator::LI_59SEC);
	updateSystemVariables(sysVar);

	DEBUG("leap second deleted.");
}

void KernelInterface::resetLeapSecond()
{
	struct timex tmx
	{
	}; //Time-struct

	tmx.modes = 0;
	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		std::stringstream errorString("Error obtaining the statusWord in KernelInterface::resetLeapSecond(): ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}

	tmx.modes = tmx.modes | MOD_STATUS;
	tmx.status = tmx.status & ~STA_DEL & ~STA_INS & ~STA_UNSYNC;
	//Insert leap second
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		std::stringstream errorString("Error resetting leap second in KernelInterface::resetLeapSecond(): ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}

	//Also set the leapIndicator in shared system variables
	SharedSystemVariables sysVar = getSystemVariables();
	sysVar.setLeapIndicator(ntp::LeapIndicator::LI_NO_WARNING);
	updateSystemVariables(sysVar);

	DEBUG("Leap second reset.");
}

ntp::LeapIndicator KernelInterface::getLeapSecond()
{
	//Old implementation, not in use because of problems with STA_UNSYNC
	// ---------------------------------------------
	/*	ntptimeval timeVal = {0};
	int retVal = 0;
	if((retVal = ntp_gettime(&timeVal)) < 0)
	{
		//Getting time unsuccessful, throw exception
		std::stringstream errorString("Error getting the time in KernelInterface::getLeapSecond(): ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}
	switch(retVal)
	{
	case TIME_WAIT:
		//Clear the Leap-Indicator flag
		resetLeapSecond();
	case TIME_OK:
	case TIME_OOP:
		INFO("Keine Warning");
		return ntp::LeapIndicator::LI_NO_WARNING;
	case TIME_INS:
		INFO("Ins");
		return ntp::LeapIndicator::LI_61SEC;
	case TIME_DEL:
		INFO("Del");
		return ntp::LeapIndicator::LI_59SEC;

	case TIME_ERROR:
	default:
		INFO("Err");
		return ntp::LeapIndicator::LI_UNKNOWN;
	}*/
	// ---------------------------------------------
	// Instead, shared system variables are used now.

	return getSystemVariables().getLeapIndicator();
}

bool KernelInterface::consoleInputAvailable()
{
	struct timeval tv;
	fd_set fds;
	tv.tv_sec = 0;
	tv.tv_usec = 0;
	FD_ZERO(&fds);
	FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
	select(STDIN_FILENO + 1, &fds, NULL, NULL, &tv);
	return FD_ISSET(STDIN_FILENO, &fds);
}

bool KernelInterface::programHasNecessaryRights()
{
	/*
	 * Perform Capability checks for the following Capablities:
	 * 		+ CAP_SYS_TIME
	 * 			- set the system clock (settimeofday, stime, adjtimex)
	 * 		+ CAP_IPC_LOCK
	 * 			- Lock memory (mlock, mmap, shmctl)
	 * 		+ CAP_NET_BIND_SERVICE
	 * 			- Bind a socket to Internet domain privileged ports (port numbers less than 1024)
	 * 		+ (maybe) CAP_NET_ADMIN
	 * 			- perform various network related operations
	 * 		+ (maybe) CAP_NET_BROADCAST
	 * 			- make some socket broadcasts and multicasts
	 * 		+ (maybe) CAP_NET_RAW
	 * 			- use RAW and PACKET sockets
	 *
	 */

	bool result = true;

	if (geteuid() != 0)
	{
		result = false;
	}

	return result;
}

std::uint32_t KernelInterface::hostToNetworkOrder(std::uint32_t figure)
{
	return htonl(figure);
}

std::uint16_t KernelInterface::hostToNetworkOrder(std::uint16_t figure)
{
	return htons(figure);
}

/*
			#####   ####   ####   ####  #####
			#    # #    # #    # #        #
			#####  #    # #    #  ####    #
			#    # #    # #    #      #   #
			#    # #    # #    # #    #   #
			#####   ####   ####   ####    #
 ------------------------------------------------------------
 */

#if !defined(NTP_USE_BOOST)
// if we do not use boost, all of the following methods will be necessary:

void KernelInterface::deletePIDFromSharedMem()
{
	//First, lock the semaphore.
	if (lockSemaphore())
	{
		//Open or create the shared memory
		int sharedFD = shm_open(SHM_NAME, O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
		if (sharedFD < 0)
		{
			int tempErrno = errno;
			std::string error = "Couldn't open the shared memory: ";
			error.append(strerror(tempErrno));
			WARN(error);
		}
		else
		{
			//Get the offset. For Clients, it's at position 0, for Servers at position 8.
			off_t offset = 0;
			switch (ntp::NTP::getMode())
			{
			case ntp::NTP::AppMode::MODE_CLIENT:
				offset = OFFSET_CLIENT_PID;
				break;
			case ntp::NTP::AppMode::MODE_SERVER:
				offset = OFFSET_SERVER_PID;
				break;
			}

			//Seek to the offset.
			if (lseek(sharedFD, offset, SEEK_SET) < 0)
			{
				int tempErrno = errno;
				if (tempErrno == EINVAL)
				{
					//The file is not that long. nothing to do.
				}
				else
				{
					std::string error = "Couldn't seek: ";
					error.append(strerror(tempErrno));
					WARN(error);
				}
			}
			else
			{
				//Write 0s over the spot.
				uint64_t toWrite = 0;
				if (write(sharedFD, (void*)&toWrite, sizeof(toWrite)) < 0)
				{
					int tempErrno = errno;
					std::string error = "Error while writing: ";
					error.append(strerror(tempErrno));
					WARN(error);
				}

				//Seek to the beginning of the file
				if (lseek(sharedFD, 0, SEEK_SET) < 0)
				{
					int tempErrno = errno;
					std::string error = "Couldn't seek: ";
					error.append(strerror(tempErrno));
					WARN(error);
				}
			}

			if (close(sharedFD) < 0) //Close the shared memory
			{
				int tempErrno = errno;
				WARN("Couldn't close the shared memory: " + strerror(tempErrno));
			}
		}

		freeSemaphore();
	}

} // void KernelInterface::deletePIDFromSharedMem()

bool KernelInterface::isOtherProcessRunning()
{
	bool result = false;

	//First, lock the semaphore.
	if (lockSemaphore())
	{
		//Open or create the shared memory
		int sharedFD = shm_open(SHM_NAME, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
		if (sharedFD < 0)
		{
			int tempErrno = errno;
			std::string error = "Couldn't open or create the shared memory: ";
			error.append(strerror(tempErrno));
			LOG_ERROR(error);
			throw(ntp::FileException(error, true));
		}
		else
		{
			//Do the checking. First, get our own PID.
			pid_t ownPID = getpid();

			//Extract the corresponding PID from the file. For Clients, it's at position 0-7, for Servers at position 8-15.
			off_t offset = 0;
			switch (ntp::NTP::getMode())
			{
			case ntp::NTP::AppMode::MODE_CLIENT:
				offset = OFFSET_CLIENT_PID;
				break;
			case ntp::NTP::AppMode::MODE_SERVER:
				offset = OFFSET_SERVER_PID;
				break;
			}

			//Seek to the offset.
			if (lseek(sharedFD, offset, SEEK_SET) < 0)
			{
				int tempErrno = errno;
				if (tempErrno == EINVAL)
				{
					//The file is not that long. we only need to truncate it to the specified offset.
					//In this case we also directly write the PID, and seek again.
					if (ftruncate(sharedFD, offset) < 0)
					{
						tempErrno = errno;
						std::string error = "Couldn't truncate the file: ";
						error.append(strerror(tempErrno));
						WARN(error);
					}
					else
					{

						if (lseek(sharedFD, offset, SEEK_SET) < 0)
						{
							int tempErrno = errno;
							std::string error = "Couldn't seek: ";
							error.append(strerror(tempErrno));
							WARN(error);
						}
					}
				}
				else
				{
					std::string error = "Couldn't seek: ";
					error.append(strerror(tempErrno));
					WARN(error);
				}
			}

			//Now, read the PID from the file
			uint64_t readPID64 = 0;
			if (read(sharedFD, &readPID64, sizeof(readPID64)) < 0)
			{
				int tempErrno = errno;
				std::string error = "Couldn't read: ";
				error.append(strerror(tempErrno));
				WARN(error);
			}

			//Compare the two
			pid_t readPID = (pid_t)readPID64;

			if (readPID == ownPID)
			{
				//Our PID is in the file, we're the active (and only) process.
				result = false; //No other process is running.
			}
			else
			{
				//Do checks on the PID. Compare our name to the other PIDs name.
				if (doProcessesHaveSameName(ownPID, readPID))
				{
					//Both have the same name - we are a duplicate!
					result = true; //Another process is running
				}
				else
				{
					//The other process has a different name - the pid in the file seems to be a thing of the past.
					//Update it with our PID.
					result = false; //No other process is running.

					//seek to the specified offset.
					if (lseek(sharedFD, offset, SEEK_SET) < 0)
					{
						int tempErrno = errno;
						std::string error = "Couldn't seek: ";
						error.append(strerror(tempErrno));
						WARN(error);
					}

					//Write PID
					uint64_t toWrite = ownPID;
					if (write(sharedFD, (void*)&toWrite, sizeof(toWrite)) < 0)
					{
						int tempErrno = errno;
						std::string error = "Error while writing: ";
						error.append(strerror(tempErrno));
						WARN(error);
					}

					//Seek to the beginning of the file
					if (lseek(sharedFD, 0, SEEK_SET) < 0)
					{
						int tempErrno = errno;
						std::string error = "Couldn't seek: ";
						error.append(strerror(tempErrno));
						WARN(error);
					}
				}
			}

			if (close(sharedFD) < 0) //Close the shared memory
			{
				int tempErrno = errno;
				WARN("Couldn't close the shared memory: " + strerror(tempErrno));
			}

			sharedMemInitialized = true;
		}

		freeSemaphore();
	}
	else //If semaphore could not be locked
	{
		result = true;
	}

	return result;
}

void KernelInterface::initSocket()
{
	//create a UDP socket
	DEBUG("Initializing socket.");
	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sockfd == -1)
	{
		//Fatal Error.
		std::string error = "The socket could not be created. Exiting the application.";
		LOG_ERROR(error);
		throw(ntp::NetworkException(error, true));
	}
	//UDP Socket created.
	INFO("UDP-Socket created.");

	if (ntp::NTP::getMode() == NTP::MODE_SERVER)
	{
		/*
		 * Only create Address-Structure and bind to port
		 * in server mode. Client-mode doesn't need to bind
		 * to a specific port.
		 */

		//Initialize Address-Structure with 0.
		sockaddr_in ownAddress{};

		//Set Address and Port
		ownAddress.sin_family = AF_INET; //This does not get our real address!
		ownAddress.sin_port = hostToNetworkOrder(ntp::Constants::getInstance().getUShort(ntp::ConstantsUShort::ntpPort));
		ownAddress.sin_addr.s_addr = hostToNetworkOrder(INADDR_ANY);

		//Bind socket to port
		if (bind(sockfd, (struct sockaddr*)&ownAddress, sizeof(ownAddress)) == -1)
		{
			//Fatal Error.
			std::string error = "Port could not be bound. Ending the application.";
			LOG_ERROR(error);
			throw(ntp::NetworkException(error, true));
		}
		//Binding to port successful
		DEBUG("Socket bound to port " +
		      std::to_string(ntohs(ownAddress.sin_port)) + ".");
	}

	//Set socket to timeout at receive-operations
	timeval timeoutVal = {static_cast<long int>(ntp::Constants::getInstance().getUInt(ntp::ConstantsUInt::recvTimeout)), 0};
	if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (void*)&timeoutVal, sizeof(timeoutVal)) < 0)
	{
		std::stringstream errorString("Failed to set ReceiveTimeout: ");
		errorString << std::strerror(errno);
		WARN(errorString.str());
	}

	socketInitialized = true;
}

void KernelInterface::closeSocket()
{
	if (socketInitialized)
	{
		if (close(sockfd) < 0)
		{
			int tempErrno = errno;
			WARN("Couldn't close the socket: " + strerror(tempErrno));
		}
		else
		{
			INFO("Closed socket.");
		}
	}
}

sockaddr_in KernelInterface::ntpToUnixIPAddress(const IPAddress& address) const
{
	sockaddr_in addr{};
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = address.getAddress();
	addr.sin_port = address.getPort();

	return addr;
}

IPAddress KernelInterface::unixToNTPIPAddress(const sockaddr_in& addr) const
{
	return IPAddress(addr.sin_addr.s_addr, addr.sin_port);
}

void KernelInterface::transmitPacket(ntp::Message& msg)
{
	sockaddr_in peerAddress = ntpToUnixIPAddress(msg.getPeerAddress());
	ntp::NTPPacket payload = msg.getPayload();

	//Construct string from vector
	std::vector<unsigned char> frame = payload.createFrame();
	std::string message(frame.begin(), frame.end());

	//Send out
	if (sendto(sockfd, frame.data(), frame.size(), 0, (struct sockaddr*)&peerAddress, sizeof(peerAddress)) == -1)
	{
		int err = errno;
		//Sending not successful
		//Throw an exception!
		char addressOfPeer[100];
		//std::stringstream errorString("Error in KernelInterface::transmitPacket: ");
		std::stringstream errorString("");
		if (inet_ntop(AF_INET, &(peerAddress.sin_addr), addressOfPeer, 100) != NULL)
		{
			std::string address(addressOfPeer);
			errorString << "Failed to transmit a message to the Peer with address " << address << ". ";
		}
		else
		{
			errorString << "Failed to transmit a message. ";
		}

		errorString << "Error cause: " << strerror(err);
		throw(ntp::NetworkException(errorString.str(), false));
	}
}

std::shared_ptr<ntp::Message> KernelInterface::receivePacket()
{
	std::shared_ptr<ntp::Message> msgPointer(nullptr);
	bool received = false;

	while (!received)
	{
		//Create struct for receiving the address
		sockaddr_in remoteAddress{};

		//clear the buffer by filling zeros, it might have previously received data
		memset(buffer, '\0', RECV_BUFLEN);

		//try to receive some data.
		socklen_t remoteAddressLength = sizeof(sockaddr_in); //From https://linux.die.net/man/2/recvfrom: "The argument addrlen is a value-result argument, which the caller should initialize before the call to the size of the buffer associated with src_addr, and modified on return to indicate the actual size of the source address."

		//This call waits until something was received. No timeout is specified.
		int packet_length;
		packet_length = recvfrom(sockfd, &buffer, RECV_BUFLEN, 0, (struct sockaddr*)&remoteAddress, &remoteAddressLength);

		if (packet_length > 0)
		{
			//Something was received, let's try to create a ntp::Message from it
			DEBUG("Paket empfangen, Länge: " + std::to_string(packet_length));

			//First, get the current timestamp
			ntp::LongTimestamp currentTimestamp = getTime();

			//Second, create a vector from the received data
			std::vector<unsigned char> receivedData(buffer, buffer + packet_length);

			//Third, call the constructor of ntp::Message
			try
			{
				received = true;
				msgPointer = std::make_shared<ntp::Message>(unixToNTPIPAddress(remoteAddress), receivedData, currentTimestamp);
			}
			catch (ntp::InvalidPacketException& e)
			{
				//Catch an InvalidPacketException:
				//The received packet did not match the NTP-Syntax.
				//Log the error and continue waiting
				LOG_ERROR(std::string("got Invalid Packet: ") + e.toString());
				received = false;
			}
		}
		else if (errno == EAGAIN || errno == EWOULDBLOCK) //Gets set if a timeout occured
		{
			//Throw a timeout-exception to the receiver
			throw(ntp::TimeoutException("Receiver timed out."));
		}
		else
		{
			std::stringstream errorString("Error occured during receiving: ");
			errorString << std::strerror(errno);
			WARN(errorString.str());
		}

		//std::this_thread::yield();	//Give other threads a chance for execution while we wait.
	} //end of while-loop

	return msgPointer;
}

IPAddress KernelInterface::getAddressFromName(std::string hostname)
{
	const addrinfo* emptyInfo = NULL;
	addrinfo* info = NULL;

	//Remove invalid characters from the string
	auto new_end = std::remove_if(hostname.begin(), hostname.end(),
	                              [](const char& character) {
		                              return !(strchr("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.", character));
	                              });

	hostname.erase(new_end, hostname.end());

	//Spawn an asynchronous operation so that we can display info that we're still doing something
	auto handle = std::async(std::launch::async, getaddrinfo, hostname.c_str(), "123", emptyInfo, &info);
	//Now wait for the function to complete and print status information every seconds
	while (handle.wait_for(std::chrono::seconds(1)) != std::future_status::ready)
	{
		DEBUG("Waiting for DNS-resolve...");
	}

	if (handle.get() == -1)
	{
		//Fail, throw error
		throw NetworkException("Hostname " + hostname + " could not be resolved.", false);
	}

	if (info == NULL || info->ai_family != AF_INET) //Only IPv4 for now
	{
		//Fail, throw error
		throw NetworkException("Hostname " + hostname + " did not resolve to an IPv4 address.", false);
	}

	DEBUG("Hostname was successfully resolved.");

	sockaddr_in address = *((sockaddr_in*)info->ai_addr);
	freeaddrinfo(info);

	//Success, return
	return unixToNTPIPAddress(address);
}

bool KernelInterface::updateSystemVariables(SharedSystemVariables newSystemVariables)
{
	bool output = false;

	this->systemVariables = newSystemVariables;

	CHATTY_UPDATE_SYSTEM_VARIABLES("Updating system variables: " + newSystemVariables.toString());
	output = true;

	//Lock the semaphore
	if (lockSemaphore())
	{
		//Open or create the shared memory
		int sharedFD = shm_open(SHM_NAME, O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
		if (sharedFD < 0)
		{
			int tempErrno = errno;
			WARN("Couldn't open the shared memory: " + strerror(tempErrno));
			output = false;
		}
		else
		{
			//Seek to the offset.
			if (lseek(sharedFD, OFFSET_SYSTEMVARIABLES, SEEK_SET) < 0)
			{
				int tempErrno = errno;
				if (tempErrno == EINVAL)
				{
					//The file is not that long. we only need to truncate it to the specified offset.
					if (ftruncate(sharedFD, OFFSET_SYSTEMVARIABLES) < 0)
					{
						tempErrno = errno;
						std::string error = "Couldn't truncate the file: ";
						error.append(strerror(tempErrno));
						WARN(error);
					}
					else
					{
						if (lseek(sharedFD, OFFSET_SYSTEMVARIABLES, SEEK_SET) < 0)
						{
							int tempErrno = errno;
							std::string error = "Couldn't seek: ";
							error.append(strerror(tempErrno));
							WARN(error);
						}
					}
				}
				else
				{
					std::string error = "Couldn't seek: ";
					error.append(strerror(tempErrno));
					WARN(error);
				}
			}

			//Write to the memory
			//Simply insert the raw data
			int size = systemVariables.getRawData().size();
			while (write(sharedFD, systemVariables.getRawData().data(), size) < size)
			{
				int tempErrno = errno;
				if (tempErrno != EAGAIN)
				{
					WARN("Couldn't write to shared memory: " + strerror(tempErrno));
					output = false;
					break;
				}
			}

			//Seek to the beginning of the file
			if (lseek(sharedFD, 0, SEEK_SET) < 0)
			{
				int tempErrno = errno;
				std::string error = "Couldn't seek: ";
				error.append(strerror(tempErrno));
				WARN(error);
			}

			//Close the shared memory
			if (close(sharedFD) < 0)
			{
				int tempErrno = errno;
				WARN("Couldn't close the shared memory: " + strerror(tempErrno));
				output = false;
			}
		}
	}

	freeSemaphore();

	return output;
}

SharedSystemVariables KernelInterface::getSystemVariables()
{
	//In Client-Mode, we can just return the internally stored object.
	if (ntp::NTP::getMode() == NTP::AppMode::MODE_CLIENT)
	{
		return systemVariables;
	}

	//In Server-Mode, we need to get it from shared memory.

	std::string error = "";

	DEBUG("Reading from shared memory.");

	if (lockSemaphore())
	{
		//Open or create the shared memory
		int sharedFD = shm_open(SHM_NAME, O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
		if (sharedFD < 0)
		{
			int tempErrno = errno;
			error = "Couldn't open the shared memory: ";
			error.append(strerror(tempErrno));
		}
		else
		{
			//Seek to the specified offset
			if (lseek(sharedFD, OFFSET_SYSTEMVARIABLES, SEEK_SET) < 0)
			{
				int tempErrno = errno;
				std::string error = "Couldn't seek: ";
				error.append(strerror(tempErrno));
				WARN(error);
			}
			else
			{

				//Read from memory
				int size = systemVariables.getRawSize();
				unsigned char* buffer = (unsigned char*)malloc(size);
				ssize_t ret = 0;
				bool success = true;

				if (buffer == nullptr)
				{
					success = false;
					std::string message = "Could not allocate memory. Size:" + std::to_string(size);
					LOG_ERROR(message);
				}
				else
				{
					while (size != 0 && (ret = read(sharedFD, buffer + ret, size)) != 0)
					{
						if (ret == -1)
						{
							int tempErrno = errno;
							if (tempErrno == EINTR)
							{
								continue;
							}
							else
							{
								error = "Couldn't read from shared memory: ";
								error.append(strerror(tempErrno));
								success = false;
								break;
							}
						}
						else
						{
							size -= ret;
						}
					}

					if (success)
					{
						//DEBUG("Success in reading the timestamp!");
						size = systemVariables.getRawSize();
						std::vector<unsigned char> rawData(buffer, buffer + size);
						//std::vector<unsigned char>::iterator it = rawData.begin();
						systemVariables.loadFromRaw(rawData);
						//DEBUG(referenceTimestamp.toString());
						DEBUG("Read shared variables: " + systemVariables.toString());
					}

					free(buffer);
				}

			} //End of seek-else

			//Close the shared memory
			if (close(sharedFD) < 0)
			{
				int tempErrno = errno;
				error = "Couldn't close the shared memory: ";
				error.append(strerror(tempErrno));
			}
		}
	}

	freeSemaphore();

	if (error != "" && NTP::getMode() == NTP::MODE_SERVER)
	{
		WARN(error);
	}

	return systemVariables;
}

#else // -> if defined( NTP_USE_BOOST )

bool KernelInterface::isOtherProcessRunning()
{
	ScopedSemaphore<boost::interprocess::named_semaphore> _s(this->shmSemaphore);

	bool result = false;
	pid_t myId = getpid();

	pid_t sharedId = 0;

	switch (ntp::NTP::getMode())
	{
	case ntp::NTP::AppMode::MODE_CLIENT:
		sharedId = *static_cast<pid_t*>(this->sharedClientProcessId.get_address());
		break;
	case ntp::NTP::AppMode::MODE_SERVER:
		sharedId = *static_cast<pid_t*>(this->sharedServerProcessId.get_address());
		break;
	}

	if (sharedId == myId)
	{
		result = false; // We are already part of that semaphore
	}
	else
	{
		if (doProcessesHaveSameName(sharedId, myId))
		{
			// process was started twice
			result = true;
		}
		else
		{
			switch (ntp::NTP::getMode())
			{
			case ntp::NTP::AppMode::MODE_CLIENT:
				*static_cast<pid_t*>(sharedClientProcessId.get_address()) = myId;
				break;
			case ntp::NTP::AppMode::MODE_SERVER:
				*static_cast<pid_t*>(sharedServerProcessId.get_address()) = myId;
				break;
			}

			result = false;
		}
	}

	return result;
}

#endif // ! defined( NTP_USE_BOOST )

#endif // defined(NTP_LINUX)

} //End of namespace ntp
