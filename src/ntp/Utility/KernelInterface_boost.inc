/*
* ----------------------------------------------------------------------------
* Copyright 2018 Ostfalia University of Applied Sciences, Germany
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* ----------------------------------------------------------------------------
*/

/**
 * @file 	KernelInterface_boost.cpp
 * @details This file implements the parts of the KernelInterface-Class, that are used when compiling with boost-support.
 * @date 	20.11.2017
 * @author 	Silvan König
 * @version	1.0
 *
 */

#include "ScopedSemaphore.hpp"
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>

#include "BoostUDPTimeoutTransmitter.hpp"

namespace ntp
{

void KernelInterface::deletePIDFromSharedMem()
{

	ScopedSemaphore<boost::interprocess::named_semaphore> _s(this->shmSemaphore);

	switch (ntp::NTP::getMode())
	{

	case ntp::NTP::AppMode::MODE_CLIENT:
		*static_cast<pid_t*>(sharedClientProcessId.get_address()) = 0;
		break;

	case ntp::NTP::AppMode::MODE_SERVER:
		*static_cast<pid_t*>(sharedServerProcessId.get_address()) = 0;
		break;
	}
}

void KernelInterface::initSocket()
{
	if (this->socketInitialized)
	{
		throw ntp::NetworkException("Socket already initialized", true);
	}

	//create a UDP socket
	DEBUG("Initializing socket.");

	using namespace boost::asio::ip;

	if (ntp::NTP::getMode() == NTP::MODE_SERVER)
	{
		auto port = ntp::Constants::getInstance().getUShort(ntp::ConstantsUShort::ntpPort);

		udp::endpoint ep{udp::v4(), port};

		try
		{
			this->udpsocket = new udp::socket{this->ioservice, ep};
		}
		catch (boost::system::system_error& e)
		{
			std::string error = "Port could not be bound. Ending the application.";
			LOG_ERROR(error + e.what());
			throw(ntp::NetworkException(error, true));
		}

		//Binding to port successful
		DEBUG(std::string("Socket bound to port ") +
		      std::to_string(ep.port()) + ".");

		socketInitialized = true;
	}
	else if (ntp::NTP::getMode() == NTP::MODE_CLIENT)
	{
		udp::endpoint ep{udp::v4(), 0};

		try
		{

			this->udpsocket = new udp::socket{this->ioservice, ep};
		}
		catch (boost::system::system_error& e)
		{
			std::string error = "Port could not be bound. Ending the application.";
			LOG_ERROR(error + e.what());
			throw(ntp::NetworkException(error, true));
		}

		//Binding to port successful
		DEBUG(std::string("Socket bound to port ") +
		      std::to_string(ep.port()) + ".");

		socketInitialized = true;
	}
}

void KernelInterface::closeSocket()
{
	try
	{
		this->udpsocket->close();
		delete this->udpsocket;
		this->socketInitialized = false;
	}
	catch (boost::system::system_error& e)
	{
		LOG_ERROR("Error: unable to close socket" + e.what());
	}
	// todo: might throw another exception asio::error::operation_aborted or similar
}

void KernelInterface::transmitPacket(ntp::Message& msg)
{
	// todo: kann das noch optimiert werden?
	using namespace boost::asio::ip;
	using boost::asio::detail::socket_ops::network_to_host_long;

	if (!this->socketInitialized)
	{
		throw ntp::NetworkException("Socket uninitialized", true);
	}

	ntp::IPAddress const& msgAddress = msg.getPeerAddress();
	address_v4 address{network_to_host_long(msgAddress.getAddress())};

	udp::endpoint ep{address, msgAddress.getPort()};

	DEBUG(msg.getPayload().toString());
	std::vector<unsigned char> frame = msg.getPayload().createFrame();
	auto buf = boost::asio::buffer(frame.data(), frame.size());

	try
	{
		this->udpsocket->send_to(buf, ep);
	}
	catch (boost::system::system_error& e)
	{
		std::string errorString{"Failed to transmit message to the Peer with address "};
		errorString += msgAddress.toString();
		LOG_ERROR(e.what());
		throw(ntp::NetworkException(errorString, false));
	}
}

std::shared_ptr<ntp::Message> KernelInterface::receivePacket()
{

	using boost::asio::detail::socket_ops::host_to_network_long;

	if (!this->socketInitialized)
	{
		LOG_ERROR("Socket uninitialized");
		throw ntp::NetworkException("Socket uninitialized", true);
	}

	unsigned int timeout = ntp::Constants::getInstance().getUInt(ntp::ConstantsUInt::recvTimeout);

	std::vector<unsigned char> buffer(RECV_BUFLEN);

	boost::asio::ip::udp::endpoint ep{};
	boost::system::error_code ec{};

	BoostUDPTimeoutTransmitter c{*this->udpsocket};

	std::size_t packet_length = c.receive(boost::asio::buffer(buffer),
	                                      boost::posix_time::seconds(timeout), ec, ep);

	buffer.resize(packet_length);
	auto ip_ad = ep.address().to_v4().to_ulong();

	IPAddress ip{host_to_network_long(ip_ad), ep.port()};

	LongTimestamp recvtime = getTime();

	Message* msg = new Message{ip, buffer, recvtime};

	std::shared_ptr<ntp::Message> msgPointer(msg);

	return msgPointer;
}

IPAddress KernelInterface::getAddressFromName(std::string hostname)
{
	using namespace boost::asio::ip;
	using boost::asio::detail::socket_ops::host_to_network_long;
	using boost::asio::ip::udp;

	udp::endpoint ep;

	try
	{
		udp::resolver resolver(this->ioservice);
		udp::resolver::query query(udp::v4(), hostname, "");

		ep = *resolver.resolve(query);
	}
	catch (boost::system::system_error& e)
	{
		throw ntp::NetworkException{std::string("Hostname ") + hostname + " could not be resolved.", false};
	}

	if (ep.port() == 0)
	{
		ep.port(123);
	}

	address ip = ep.address();

	uint32_t ipAsInt = host_to_network_long(ip.to_v4().to_ulong());

	return IPAddress(ipAsInt, ep.port());
}

bool KernelInterface::updateSystemVariables(SharedSystemVariables newSystemVariables)
{
	CHATTY_UPDATE_SYSTEM_VARIABLES("Updating system variables: " + newSystemVariables.toString());

	ScopedSemaphore<boost::interprocess::named_semaphore> _s(this->shmSemaphore);

	SharedSystemVariables* svar = nullptr;

	svar = static_cast<SharedSystemVariables*>(this->sharedSystemVariablesRegion.get_address());

	if (svar == nullptr)
	{

		return false;
	}

	*svar = newSystemVariables;
	this->systemVariables = newSystemVariables;

	bool output = true;
	return output;
}

SharedSystemVariables KernelInterface::getSystemVariables()
{
	SharedSystemVariables s;

	s = *static_cast<SharedSystemVariables*>(this->sharedSystemVariablesRegion.get_address());

	return s;
}

} //End of namespace ntp
