/*
* ----------------------------------------------------------------------------
* Copyright 2018 Ostfalia University of Applied Sciences, Germany
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* ----------------------------------------------------------------------------
*/

/**
 * @file 	KernelInterface.hpp
 * @details This Header declares the KernelInterface Class.
 * @date 	05.06.2016
 * @author 	Christian Jütte, Silvan König
 * @version	2.0
 */

#ifndef NTP_UTILITY_KERNELINTERFACE_HPP_
#define NTP_UTILITY_KERNELINTERFACE_HPP_

#if defined(__WIN32) || defined(__WIN32__) || defined(WIN32) || defined(_WIN32)

#define NTP_WINDOWS 1
#define NTP_USE_BOOST 1
#ifdef _MSC_VER
#include <sdkddkver.h> // _WIN32_WINNT
#endif

#endif // if a compiler for windows was detected

#if defined(linux) || defined(__linux)

#define NTP_LINUX

#endif

#include "../Datatypes/Exceptions/FileException.hpp"
#include "../Datatypes/Exceptions/InvalidPacketException.hpp"
#include "../Datatypes/Exceptions/NetworkException.hpp"
#include "../Datatypes/Exceptions/TimeException.hpp"
#include "../Datatypes/Exceptions/TimeoutException.hpp"
#include "../Datatypes/Frame/Message.hpp"
#include "../Datatypes/IPAddress.hpp"
#include "../Datatypes/SharedSystemVariables.hpp"
#include "Constants.hpp"

#if defined(NTP_USE_BOOST)
#include <boost/asio.hpp>

#if defined(NTP_WINDOWS)
typedef DWORD pid_t;
#include "PhaseFrequencyLockLoop.hpp"
#include <boost/interprocess/windows_shared_memory.hpp>
#else
#include <boost/interprocess/shared_memory_object.hpp>
#endif

#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/sync/named_semaphore.hpp>
#endif

#if defined(NTP_LINUX)
#include <netinet/in.h>
#include <sys/timex.h> //NTP-Functions
#include <sys/types.h> //pid_t
#endif

namespace ntp
{

/**
 * @brief The class KernelInterface provides the rest of the application with interfaces to OS-specific functions.
 *
 * ## General Description
 *
 * The KernelInterface contains a collection of utility-functions used at various places in the program.
 * It's main purpose is to abstract functions from OS-specific functions. It provides the rest of the application
 * with a universal interface to these functionalities. To compile this NTP implementation for different operating
 * systems, the KernelInterface is the only part of the code that needs to be adapted, since it uses
 * system-specific functions. The functions can be grouped into the following sections:
 *
 * - _Networking_: Opening and closing sockets; Transmitting and receiving packets; Obtaining addresses
 * - _Time Manipulation_: Obtaining the system time; Manipulating the system time in accordance with NTP;
 *   Obtaining NTP-statistics, Obtaining the running time of the process
 * - _IPC, Shared Memory_: Sharing system-data between a server and a client running on the same machine
 * - System-specific console-IO applications
 *
 * Basic descriptions of what can be done with the KernelInterface can be found below.
 * More detailed information on what exactly the functions do can be obtained from the function descriptions.
 *
 * ## Functionality
 * #### Networking
 * The KernelInterface manages the UDP-Socket of the program and provides functions to initialize it,
 * transmit and receive packets through it and to close it. It also stores the host's IP-Address.
 *
 * #### Time Manipulation
 * This topic can be further divided into two parts: System Time and Process Time.
 *
 * The KernelInterface contains functions to obtain the system time, and to manipulate it in accordance with NTP.
 * The system time is the regular "wall-clock"-time, which has to be synchronized to other clocks.
 *
 * The process time in contrast is specific to each process: It is the time for which a process has been running.
 * In this implementation it is used to be able to compare the time of events, even if the system time is changed
 * between the events. The KernelInterface provides functions to obtain the ProcessTime and to convert it to some handy
 * datatypes.
 *
 * It should be said here that for an adaptation of the KernelInterface to a different system, the functions for the process
 * time do likely not need to be changed, since they rely on the C++-Standard.
 *
 * #### IPC, Shared Memory
 * This implementation can only run as either server or client, not both at the same time. A server and a client
 * can however run simultaneously if the application is started twice, once configured as client, and once as server.
 *
 * NTP defines some system variables, that are created by the client, but required by the server to send out to other
 * clients. The KernelInterface provides a way to do this in this implementation. It also provides a way to not let
 * two clients run on the same machine on the same time, so that they don't conflict with each other.
 *
 * For the communication, a designated named shared memory area is used. All access to it is protected by a semaphor,
 * which is also named. The shared memory contains the following items:
 *
 * Bytes				|	Content
 * ---------------------|----------
 * 0-7					| PID of the Client-process
 * 8-15					| PID of the Server-process
 * 16-34 (currently) 	| Shared System Variables
 *
 * At the start of the program (in NTP.cpp), the function `isOtherProcessRunning` is called. This opens the shared memory
 * and checks if a PID is already entered in the spot for the corresponding running mode. For example, if a server was started,
 * it will look if there is a PID at the server spot (Bytes 8-15). If a PID exists, it doesn't match the current processes PID,
 * and the process which the PID from the shared memory belongs to is named the same as the current process,
 * then another process of NTP is already running. Execution should be aborted.
 *
 * After an successful initialization, a program running in Client-Mode will update the system variables each time it
 * synchronizes the clock. A server-mode program will get these variables from shared memory when it want's to send a packet.
 * For convenience, the class SharedSystemVariables is used to easily store and recover the variables to and from their binary
 * form in memory.
 *
 * ## Usage
 *
 * The KernelInterface is implemented as a singleton, which means that it can only be constructed once. This is
 * done in the beginning of NTP.cpp. To use it, one must obtain the single instance via KernelInterface::getInstance().
 *
 * Of course a reference to the singleton-object can be stored for more convenient use. This is best done by defining a
 * member variable such as
 *
 * 			private: ntp::KernelInterface& kernelInterface;
 *
 * which is then initialized upon construction
 *
 * 			Class::Class() : kernelInterface(ntp::KernelInterface::getInstance())
 * 			{}
 *
 */
class KernelInterface
{
private:
	///@name Constants for shared memory:
	///@{
	static constexpr unsigned int PID_SIZE = 8;
	static constexpr unsigned int OFFSET_CLIENT_PID = 0;
	static constexpr unsigned int OFFSET_SERVER_PID = 8;
	static constexpr unsigned int OFFSET_SYSTEMVARIABLES = 16;
	static constexpr const char* SHM_NAME = "/ntp4ntsshared"; ///<Constant for the name of the shared memory and semaphore.
	///@}

	/**
	 *  @brief		This is the Difference between Linux and Windows Timestamps in seconds.
	 *	@details	obtained from http://www.bluethinking.com/?p=167
	 */
	static constexpr unsigned long long UNIX_WINDOWS_TIME_DIFFERENCE_SECONDS = 11644473600;

#if defined(NTP_USE_BOOST) || _MAKE_DOCUMENTATION

	///@name Variables for network-communication
	///@{
	boost::asio::io_service ioservice;       /// used when compiling with boost
	boost::asio::ip::udp::socket* udpsocket; /// used when compiling with boost

/**
 * @brief Used when compiling for boost. Have a look at the Details!
 * @details	When compiled for Windows, shm will be a windows_shared_memory object and
 * 	under Linux this will be a shared_memory_object.
 */
#if defined(NTP_WINDOWS) || _MAKE_DOCUMENTATION
	boost::interprocess::windows_shared_memory shm;
#else
	boost::interprocess::shared_memory_object shm;
#endif

#endif // defined(NTP_USE_BOOST)

#if defined(NTP_LINUX) || _MAKE_DOCUMENTATION

	/**
		 * @brief	Socket file descriptor for native Linux.
		 */
	int sockfd;

#endif

	///Length of the receive-buffer
	static const std::size_t RECV_BUFLEN = 65507;
	///Receive-Buffer
	unsigned char buffer[RECV_BUFLEN];
	///Flag to know if the socket was initialized
	bool socketInitialized = false;
	///@}

#if defined(NTP_WINDOWS) || _MAKE_DOCUMENTATION
	/// Windows-only: implementation of the PLL/FLL-algorithm
	PhaseFrequencyLockLoop pfll;
	/// Windows-only: keep the leap second in mind
	signed char leapSecond;
#endif

	///Time when the program was started.
	std::chrono::steady_clock::time_point startTime;

	///Singleton-Object of this class
	static ntp::KernelInterface instance;

	///System Precision (updated at each adjustTime-Call) in log2-Seconds
	signed char systemPrecision;
	///Time constant for the PLL. See RFC 1589, p.29 for more info.
	unsigned int TIME_CONSTANT = 0;

	/**
	 * @name Shared memory and semaphore
	 * @{
	 */
#if defined(NTP_USE_BOOST) || _MAKE_DOCUMENTATION

	/// used when compiling with boost
	boost::interprocess::mapped_region sharedSystemVariablesRegion;
	/// used when compiling with boost
	boost::interprocess::mapped_region sharedServerProcessId;
	/// used when compiling with boost
	boost::interprocess::mapped_region sharedClientProcessId;
	/// used when compiling with boost
	boost::interprocess::named_semaphore shmSemaphore;

#endif // defined(NTP_USE_BOOST)

	/// Shared system variables are stored in Client-Mode for fast access.
	SharedSystemVariables systemVariables;

	/// Flag to know if shared memory was initialized
	bool sharedMemInitialized = false;
	/**
	 * @}
	 */

	/*
		 #    # ###### ##### #    #  ####  #####   ####
		 ##  ## #        #   #    # #    # #    # #
		 # ## # #####    #   ###### #    # #    #  ####
		 #    # #        #   #    # #    # #    #      #
		 #    # #        #   #    # #    # #    # #    #
		 #    # ######   #   #    #  ####  #####   ####
  -------------------------------------------------------------------
*/

	KernelInterface();

	/// The copy-constructor is deleted explicitly since we don't want to copy the KernelInterface
	KernelInterface(const KernelInterface&) = delete;

	~KernelInterface();

#if defined(NTP_LINUX) || _MAKE_DOCUMENTATION
	// Some utility functions for using Linux semaphores
	bool lockSemaphore();
	bool freeSemaphore();
#endif // defined(NTP_LINX)

	bool doProcessesHaveSameName(pid_t processOne, pid_t processTwo);
	void deletePIDFromSharedMem();

	timeval ntpToUnixTimestamp(const Timestamp& tmstmp) const;

#if defined(NTP_LINUX) || _MAKE_DOCUMENTATION
	LongTimestamp unixToNTPTimestamp(const timeval& time) const;

	sockaddr_in ntpToUnixIPAddress(const IPAddress& address) const;
	IPAddress unixToNTPIPAddress(const sockaddr_in& addr) const;
#endif

public:
	static ntp::KernelInterface& getInstance();

	///@name Networking
	///@{
	void initSocket();
	void closeSocket();
	void transmitPacket(ntp::Message& msg);
	std::shared_ptr<ntp::Message> receivePacket();
	IPAddress getAddressFromName(std::string hostname);
	///@}

	///@name Time Manipulation
	///@{
	ntp::LongTimestamp getTime();
	void adjustTime(double offset);
	void setFrequencyOffset(double frequencyOffset);
	double getFrequencyOffset();
	void setTime(double offset);
	signed char getSystemPrecision();
	double getJitter();
	double getOffset();

	std::chrono::steady_clock::time_point getProcessTime();
	unsigned long getProcessTimeMS();
	double calcTimeDifference(std::chrono::steady_clock::time_point tp1,
	                          std::chrono::steady_clock::time_point tp2);
	double processTimeToDouble(std::chrono::steady_clock::time_point tp);
	///@}

	bool isOtherProcessRunning();

	///@name System Variables
	///@{
	// Functions for the system Variables (exchanged between the client and the server program using shared memory
	bool updateSystemVariables(SharedSystemVariables systemVariables);
	SharedSystemVariables getSystemVariables();

	//Functions for leap seconds
	void insertLeapSecond();
	void deleteLeapSecond();
	void resetLeapSecond();
	ntp::LeapIndicator getLeapSecond();
	///@}

	///@name Console I/O
	///@{
	bool consoleInputAvailable();
	///@}

	static std::uint32_t hostToNetworkOrder(std::uint32_t figure);
	static std::uint16_t hostToNetworkOrder(std::uint16_t figure);

	static bool programHasNecessaryRights();

}; //End of class KernelInterface

} //end of namespace ntp

#endif /* NTP_UTILITY_KERNELINTERFACE_HPP_ */
