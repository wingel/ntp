/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	NtsInterface.hpp
 * @details This Header declares the NtpNtsInterface Class, which is the interface between NTP and NTS
 * @date 	12.08.2016
 * @author 	Simon Häußler
 * @version	0.1
 */

#ifndef NTP_NTSINTERFACE_HPP_
#define NTP_NTSINTERFACE_HPP_

#include "Datatypes/Frame/Message.hpp"
#include "Datatypes/Frame/NTPPacket.hpp"
#include "Utility/KernelInterface.hpp"

#include <NtpNtsInterface.h>

/**
 * @brief Class to implement an interface between NTP and NTS
 * @details This interface with a dedicating NTP Message in it is given to the NtsUnicastSevrice to
 * 			generate a request to the Server or to validate a incoming Message and create a request.
 * 			There are four flags in the interface to communicate between NTP and NTS. The description of
 * 			the flags is shown below.
 */
class NtsInterface : public NtpNtsInterface
{
private:
	///@details In client mode, this flag allows the NTP program to use this NTP Message for time synchronization.
	bool useMessageForTimeSync;

	///@details In every mode of the program, this flag is used to send out a NTP
	///			Message after NTS::createRequest or NTS::processingNTPMessage call.
	bool sendResponseMessage;

	///@details This flag is only used in client mode. Is a received NTP Message requires a request to the Server
	///			which is send out immediately, this flag will set to true from the NTS service.
	bool generateResponseMessage;

	///embedded NTPPacket to handle in methods in this interface
	ntp::Message& message;

	//with references, because an interface-object is only for one message-object
	ntp::NTPPacket& ntpPacket;

public:
	NtsInterface(ntp::Message& msg);
	~NtsInterface(void);

	///@name Timestamp functions
	///@{
	double getTransmitTimestamp(void) const;
	double getOriginTimestamp(void) const;
	void updateTransmitTimestamp(void);
	///@}

	std::string getIpAddress(void) const;
	std::vector<unsigned char> getSerializedPackage(void) const;

	///@name Extension Field functions
	///@{
	unsigned int getExtensionFieldCount(void) const;
	unsigned short getExtensionFieldType(unsigned int index) const;
	std::vector<unsigned char> getExtensionFieldValue(unsigned int index) const;
	void addExtensionField(unsigned short fieldType, std::vector<unsigned char> value);
	void delExtensionField(unsigned int index);
	///@}

	///@name Getter/Setter functions
	///@{
	void setUseMessageForTimeSync(bool state);
	bool getUseMessageForTimeSync(void) const;

	void setSendResponseMessage(bool state);
	bool getSendResponseMessage(void) const;

	void setGenerateResponseMessage(bool state);
	bool getGenerateResponseMessage() const;
	///@}
};

#endif /* NTP_NTSINTERFACE_HPP_ */
