/**
 * @file 	ConsoleInterface.cpp
 * @details This file implements the ConsoleInterface-Class.
 * @date 	11.12.2016
 * @author 	Christian Jütte
 * @version	0.1
 */

#include "ConsoleInterface.hpp"
//#include "../NTP.hpp"

namespace ntp
{

/**
 * @brief Constructor of the ConsoleInterface for Server-Mode
 *
 */
ntp::ConsoleInterface::ConsoleInterface(ntp::Receiver& receiver)
    : kernelInterface(ntp::KernelInterface::getInstance()),
      receiver(&receiver), transmitter(nullptr), synchronizer(nullptr), mode(NTP::getMode())
{
	DEBUG("Console-IO initialized.");
}

/**
 * @brief Constructor of the ConsoleInterface for Client-Mode
 *
 */
ntp::ConsoleInterface::ConsoleInterface(ntp::Receiver& receiver, ntp::Transmitter& transmitter, ntp::Synchronizer& synchronizer)
    : kernelInterface(ntp::KernelInterface::getInstance()),
      receiver(&receiver), transmitter(&transmitter), synchronizer(&synchronizer), mode(NTP::getMode())
{
	DEBUG("Console-IO initialized.");
}

/**
 * @brief Destructor of the ConsoleInterface
 *
 */
ntp::ConsoleInterface::~ConsoleInterface()
{
}

/**
 * @brief 	Function to use when starting a thread.
 * 		 	Repeatedly checks if an exception-pointer has been set
 * 		 	and calls processConsoleInput().
 *
 */
void ntp::ConsoleInterface::operator()()
{
	DEBUG("Console-IO-Thread started.");
	std::cout << "NTP >> " << std::flush;
	while (!(NTP::getExceptionPtr()))
	{
		try
		{
			processConsoleInput();
		}
		catch (...)
		{
			//Set global exception-pointer
			NTP::setExceptionPtr(std::current_exception());
		}
	} //End of while
}

/**
 * @brief	Checks if any characters are available to be processed,
 * 			and if yes, calls processCommand().
 * 			Sleeps for 250 miliseconds on each turn.
 *
 */
void ntp::ConsoleInterface::processConsoleInput()
{
	//Check if anything is in the input buffer
	if (kernelInterface.consoleInputAvailable())
	{
		//Handle whatever was there.
		std::string input;
		std::getline(std::cin, input);
		processCommand(input);
		std::cout << "NTP >> " << std::flush;
	}
	// Sleep a bit
	std::this_thread::sleep_for(std::chrono::microseconds(250000));
}

/**
 * @brief	Takes the console-input and processes it.
 *
 * @details	The function looks if a known command has been entered. If so, it executes the corresponding code.
 * 			The currently supported commands can be found in the class description.
 *
 * 			Further commands can simply be added by expanding the if/else-chain.
 * 			If additional arguments are needed for the commands, they should be separated by spaces.
 * 			They will be in the other fields of strArray.
 *
 * 			Please also make sure to expand the list in the header when adding new commands.
 *
 * @param [in]	input	A line of console-input.
 *
 */
void ntp::ConsoleInterface::processCommand(std::string& input)
{
	if (input.empty())
	{
		return;
	}
	//First, split the string into substrings that are separated by spaces, and store them in strArray
	std::vector<std::string> strArray;
	std::stringstream ss(input);
	std::string temp;
	while (ss >> temp)
	{
		strArray.push_back(temp);
	}

	if (strArray.size() <= 0)
	{
		return;
	}

	//Convert the first entry to lowercase
	strArray[0] = toLowerCase(strArray[0]);
	//Compare the first entry to the supported commands
	if (strArray[0] == "quit")
	{
		outputLine("Exiting the application.");
		//Throw an exception and set the exception-pointer.
		try
		{
			throw std::runtime_error("Exit by user.");
		}
		catch (...)
		{
			//Set global exception-pointer
			NTP::setExceptionPtr(std::current_exception());
		}
	}
	else if (strArray[0] == "hello")
	{
		outputLine("Hello User, welcome to the best NTP-NTS-Application in the world!");
	}
	else if (strArray[0] == "help")
	{
		if (strArray.size() > 1)
		{
			if (strArray[1] == "hello")
			{
				outputLine("The command \"hello\" simply outputs a nice welcome-message. It is used for test-purposes.");
			}
			else if (strArray[1] == "quit")
			{
				outputLine("The command \"quit\" terminates the application.");
			}
			else if (strArray[1] == "help")
			{
				outputLine("The command \"help\" is used to show all available commands and show information about the specific commands when provided with a command as an argument.");
			}
			else if (strArray[1] == "setsync")
			{
				outputLine("Adjusts the level at which synchronization is performed according to the argument passed.");
				outputLine("Possible Values:");
				outputLine("\t0\tThe time will not be adjusted");
				outputLine("\t1\tThe time will be adjusted using NTP mechanics (Filter, Select, Cluster, Combine)");
			}
			else if (strArray[1] == "setloglevel")
			{
				outputLine("Sets which types of logging messages are output on the console.");
				outputLine("A level will always also include the levels below it.");
				outputLine("For example when selecting the level \"Info\", also");
				outputLine("messages of the level \"Error\" will be displayed.");
				outputLine("The available levels are:");
				outputLine("\tDebug");
				outputLine("\tWarn");
				outputLine("\tInfo");
				outputLine("\tError");
			}
			else
			{
				outputLine("This command does not currently exist.");
			}
		}
		else
		{
			outputLine("Type \"help <command>\" to display help for a specific command.");
			outputLine("Available commands:");
			outputLine("\thello");
			outputLine("\thelp");
			outputLine("\tquit");
			outputLine("\tsetsync");
			outputLine("\tsetloglevel");
		}
	}
	else if (strArray[0] == "setsync")
	{
		if (synchronizer != nullptr)
		{
			if (strArray.size() > 1)
			{
				if (strArray[1] == "0")
				{
					//Turn off synchronizing
					synchronizer->setSync(false);
					outputLine("Turned synchronizing off (packets will still be sent).");
				}
				else if (strArray[1] == "1")
				{
					//Turn on synchronizing
					synchronizer->setSync(true);
					outputLine("Turned synchronizing on.");
				}
				else
				{
					outputLine("Invalid argument supplied. Type help setsync to get an overview of the command.");
				}
			}
			else
			{
				outputLine("No argument supplied. Type help setsync to get an overview of the command.");
			}
		}
		else
		{
			outputLine("This command is only supported in client-mode.");
		}
	}
	else if (strArray[0] == "setloglevel")
	{
		if (strArray.size() > 1)
		{
			strArray[1] = toLowerCase(strArray[1]);
			int loglevel;
			try
			{
				loglevel = Logger::logLevelFromString(strArray[1]);
				Logger::getInstance().setLogLevel(loglevel);
			}
			catch (...)
			{
				outputLine("Invalid argument supplied. Type help setloglevel to get an overview of the command.");
			}
		}
		else
		{
			outputLine("No argument supplied. Type help setloglevel to get an overview of the command.");
		}
	}
	else
	{
		outputLine("Unsupported command.");
	}
}

/**
 * @brief	Converts all characters of the given string to lowercase characters and returns the new string.
 * 			The incoming string is not changed.
 *
 * @param	str		A reference to the string to be converted.
 *
 * @returns	The new string with all-lowercase letters
 *
 */

std::string ntp::ConsoleInterface::toLowerCase(std::string& str)
{
	std::string output = str;
	for (int i = 0; str[i]; i++)
	{
		output[i] = tolower(str[i]);
	}
	return output;
}

/**
 * @brief	Short function to output just a single line with a line-break at the end.
 *
 * @param	str	The string to be printed.
 *
 * @remarks	Maybe change to call-by-reference and make an overloaded function for const char *.
 */
void ntp::ConsoleInterface::outputLine(std::string str)
{
	std::cout << str << std::endl
	          << std::flush;
}

} //End of namespace ntp
