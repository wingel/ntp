/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
* @file      Message.hpp
* @brief     Message header
*
* @version   0.4
* @date      22.05.2016
* @author    Thorben Kompa
* @copyright ---
*
* @details   ---
*/

#ifndef NTP_DATATYPES_FRAME_MESSAGE_HPP_
#define NTP_DATATYPES_FRAME_MESSAGE_HPP_

#include "../IPAddress.hpp"
#include "NTPPacket.hpp"

namespace ntp
{

class Message
{
private:
	IPAddress peerAddress;
	NTPPacket NTPpayload;
	LongTimestamp timestamp;

public:
	Message(const IPAddress&, const std::vector<unsigned char>&, const LongTimestamp);
	Message(const IPAddress&, const NTPPacket&);
	~Message(void);
	const IPAddress& getPeerAddress(void) const;
	NTPPacket& getPayload(void);
	LongTimestamp getDestTimestamp(void);
};

} // namespace ntp

#endif
