/**
 * @file    Message.cpp
 * @details Message type definition
 * @date    22.05.2016
 * @author  Thorben Kompa
 * @version 0.4
 */

#include "Message.hpp"

namespace ntp
{

/**
* @brief      Creates a Message
*
* @param[in]  peerAddress          - sockaddr_in&
* @param[in]  NTPpayload           - std::vector<unsigned char>&
* @param[in]  timestamp			   - LongTimestamp
*
* @return     ---
*
* @see        ---
*/
Message::Message(const IPAddress& peerAddress,
                 const std::vector<unsigned char>& NTPpayload,
                 const LongTimestamp timestamp)
{
	this->peerAddress = peerAddress;

	this->NTPpayload.loadFromUDP(NTPpayload);

	this->timestamp = timestamp;
}

/**
* @brief      Creates a Message
*
* @param[in]  peerAddress - sockaddr_in&
* @param[in]  NTPpayload  - NTPPacket&
*
* @return     ---
*
* @see        ---
*/
Message::Message(const IPAddress& peerAddress, const NTPPacket& NTPpayload)
{
	this->peerAddress = peerAddress;

	this->NTPpayload = NTPpayload;
}

/**
* @brief      Destructs a Message
*
* @return     ---
*
* @see        ---
*/
Message::~Message(void)
{
}

/**
* @brief      Returns the peerAddress of the Message
*
* @return     sockaddr_in&
*
* @see        ---
*/
const IPAddress& Message::getPeerAddress(void) const
{
	return peerAddress;
}

/**
* @brief      Returns the NTPpayload of the Message
*
* @return     NTPPacket&
*
* @see        ---
*/
NTPPacket& Message::getPayload(void)
{
	return NTPpayload;
}

/**
* @brief      Returns the destinationTimestamp of the Message
*
* @return     LongTimestamp
*
* @see        ---
*/
LongTimestamp Message::getDestTimestamp(void)
{
	return timestamp;
}

} // namespace ntp
