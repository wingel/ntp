/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file SurvivorCandidate.hpp
 * @brief
 * @details
 *
 * @date 19.06.2016
 * @author Christian Jütte
 * @version 0.1
 *
 * @copyright Copyright 2016, Christian Jütte
 * 			  All rights reserved.
 */

#ifndef NTP_DATATYPES_SURVIVORCANDIDATE_HPP_
#define NTP_DATATYPES_SURVIVORCANDIDATE_HPP_

#include "Peer/Peer.hpp"
#include <iostream>

namespace ntp
{

class SurvivorCandidate
{

private:
	///Pointer to the Peer associated with this tuple
	ntp::Peer* peer;

	///Offset of the Peer (theta)
	double offset;

	///Jitter of the Peer (psi)
	double jitter;

	///Merit-Factor (lambda_p) as calculated from stratum and root sync distance
	double meritFactor;

public:
	SurvivorCandidate(ntp::Peer* peer);
	~SurvivorCandidate();
	bool operator<(const SurvivorCandidate& tuple) const;
	ntp::Peer* getPeer();
	double getOffset();
	double getJitter();
	double getMeritFactor();

}; //end of class SurvivorCandidate

} //end of namespace ntp

#endif /* NTP_DATATYPES_SURVIVORCANDIDATE_HPP_ */
