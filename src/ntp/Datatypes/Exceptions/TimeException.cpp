/**
 * @file 	TimeException.cpp
 * @date 	05.06.2016
 * @author 	Christian Juette
 * @version	1.0
 *
 */

#include "TimeException.hpp"

namespace ntp
{

/**
 * @brief	Constructor of the TimeException.
 *
 * @param	[in]	error	A string containing a description of the error.
 *
 */

TimeException::TimeException(std::string error) noexcept
{
	this->error = error;
}

/**
 *
 * @brief	Returns a description of the Error that has happened.
 *
 * @returns	String describing the error.
 *
 */

std::string TimeException::toString() const noexcept
{
	return ("TimeException: " + error);
}

} // end of namespace ntp
