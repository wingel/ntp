/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	Timestamp.hpp
 * @details This Header declares the basic NTP Timestamp.
 * @date 	21.05.2016
 * @author 	silvan
 * @version	0.2
 */

#ifndef NTP_DATATYPES_TIMESTAMP_TIMESTAMP_HPP_
#define NTP_DATATYPES_TIMESTAMP_TIMESTAMP_HPP_

#include "../../Log/Logger.hpp"
#include <cstdint>
#include <ctime>
#include <string>
#include <vector>

/// abbreviation
typedef std::vector<unsigned char>::iterator vector_uchar_iter;

namespace ntp
{

/**
 * @brief	This is the base class of any NTP timestamp
 * @details It supports several operations and is designed to
 * 			be easily used within NTP packets and NTP algorithms.
 * 			Therefore, according to RFC5905, an NTP timestamp currently starts
 * 			counting in year 1900, which is the first era in NTP time calculations.
 * @warning	Unix based Systems start counting in year 1970. If you want to create a
 * 			NTP timestamp from a Unix timestamp use Timestamp(const timeval &time).
 * 			To get an Unix timestamp from a NTP timestamp use getUnixTimestamp();
 */
class Timestamp
{
protected:
	uint32_t seconds;
	uint32_t fraction;

public:
	/**
	 * @brief 	defines the difference between Unix-time and NTP-time
	 * @details Unixtime is considered to start 1. January 1970 00:00 and
	 *  		NTP starts 1. January 1900 0:00.
	 *
	 *  		This value is stated in RFC5905 figure 4.
	 */
	static constexpr unsigned long UNIX_NTP_TIME_OFFSET = 2208988800;

	/// defines the resolution of the 32bit fraction field
	static constexpr double NTPLONG_FRAC_RESOLUTION = (1.0 / UINT32_MAX); //seconds

	/// defines the resolution of the 16bit fraction field
	static constexpr double NTPSHORT_FRAC_RESOLUTION = (1.0 / UINT16_MAX); //seconds

	/// defines the year, where the struct tm starts counting
	static constexpr unsigned int TM_START_YEAR = 1900;

	Timestamp();
	Timestamp(double time);
	Timestamp(unsigned int seconds, unsigned int fraction);
	virtual ~Timestamp();
	operator double() const;
	virtual std::vector<unsigned char> getRaw() const = 0;
	virtual void loadFromRaw(vector_uchar_iter& start) = 0;
	std::string toString(void) const;
	uint32_t getSeconds() const;
	uint32_t getFraction() const;
};

} // end of namespace ntp

#endif /* NTP_DATATYPES_TIMESTAMP_TIMESTAMP_HPP_ */
