/*
 * @brief	Implementation of class IPAddress
 */

#include "IPAddress.hpp"

namespace ntp
{

/**
 * @brief	Standard-constructor, initializes to zero.
 *
 */
IPAddress::IPAddress() : address(0), port(0)
{
}

/**
 * @brief	Constructor. Initializes with the given values.
 *
 */
IPAddress::IPAddress(uint32_t addr, uint16_t prt) : address(addr), port(prt)
{
}

/**
 * @brief	Destructor
 *
 */
IPAddress::~IPAddress() {}

/**
 * @brief	Returns the IPv4-address in the common dotted-decimal notation, without the UDP-Port.
 *
 * Use this if you want to, for example, use the address in a filename.
 *
 */
std::string IPAddress::toStringNoPort() const
{
	// the following lines will create a string with the IP in a human-readable format
	unsigned char* newIPchar = (unsigned char*)&address;
	std::string newIPstring;

	newIPstring = std::to_string(newIPchar[0]) + "." + std::to_string(newIPchar[1]) + "." + std::to_string(newIPchar[2]) + "." + std::to_string(newIPchar[3]);

	return newIPstring;
}

/**
 * @brief	Returns the IPv4-address and port in the common dotted-decimal notation.
 *
 */
std::string IPAddress::toString() const
{
	// the following lines will create a string with the IP in a human-readable format
	unsigned char* newIPchar = (unsigned char*)&address;
	std::string newIPstring;

	newIPstring = std::to_string(newIPchar[0]) + "." + std::to_string(newIPchar[1]) + "." + std::to_string(newIPchar[2]) + "." + std::to_string(newIPchar[3]) + ":" + std::to_string(port);

	return newIPstring;
}

/**
 * @brief	Returns the IPv4-address
 *
 */
uint32_t IPAddress::getAddress() const
{
	return address;
}

/**
 * @brief	Returns the UDP-Port
 *
 */
uint16_t IPAddress::getPort() const
{
	return port;
}

} //end of namespace ntp
