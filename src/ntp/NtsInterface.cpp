/**
 * @file 	NtsInterface.cpp
 * @details This file implements the Interface between NTP and NTS
 * @date 	12.08.2016
 * @author 	Simon Häußler
 * @version	0.1
 */

#include "NtsInterface.hpp"

/**
 * @param[in] msg ntp::Message&. The actual Message object as a reference.
 */
NtsInterface::NtsInterface(ntp::Message& msg) : message(msg), ntpPacket(msg.getPayload())
{
	useMessageForTimeSync = false;
	sendResponseMessage = false;
	generateResponseMessage = false;
}

// Destructor, do not do anything.
NtsInterface::~NtsInterface(void) {}

/**
 * @brief Method to get the transmit timestamp of the actual NTPPacket.
 * @details This method readout the transmit timestamp from the NTPPacket as a LongTimestamp object.
 * 			This will cast to a double value and return to the calling method.\n
 * 			Info: The transmit timestamp contains the moment when the NTPPacket send back to the client.
 *
 * @return Returns a double value. It contains the transmit timestamp.
 */
double NtsInterface::getTransmitTimestamp(void) const
{
	return static_cast<double>(ntpPacket.getTransmitTime());
}

/**
 * @brief Method to get the origin timestamp of the actual NTPPacket.
 * @details This method readout the origin timestamp from the NTPPacket as a LongTimestamp object.
 * 			This will cast to a double value and return to the calling method.\n
 * 			Info: The origin timestamp contains the moment when the first NTPPacket send out to a server.
 *
 * @return Returns a double value. It contains the origin timestamp.
 */
double NtsInterface::getOriginTimestamp(void) const
{
	return static_cast<double>(ntpPacket.getOriginTime());
}

/**
 * @brief Method to get the IPv4 address of the actual message.
 * @details
 *
 * @return Returns a string value. It contains the IPv4 address in format '000.000.000.000'.
 */
std::string NtsInterface::getIpAddress(void) const
{
	return message.getPeerAddress().toStringNoPort();
}

/**
 * @brief the transmit timestamp will update now.
 * @details
 */
void NtsInterface::updateTransmitTimestamp(void)
{
	try
	{
		ntpPacket.setTransmitTime(ntp::KernelInterface::getInstance().getTime());
	}
	catch (ntp::TimeException& ex)
	{
		LOG_ERROR("Could not set the Time in NtsInterface::updateTransmitTimestamp(), because: " + ex.toString());
		INFO("Due to TimeException in NtsInterface::updateTransmitTimestamp() set the transmit timestamp to a default value.");

		ntpPacket.setTransmitTime(ntp::LongTimestamp());
	}
}

/**
 * @brief Method to get the amount of the ExtensionFields in the list of the actual NTPPacket.
 * @details This method temporary stores the ExtensionField list and returns the size of the
 * 			ExtensionField list of the actual NTPPacket..
 *
 * @return Returns an integer value. It contains the amount of ExtensionField objects in the
 * 			ExtensionField list of the actual NTPPacket.
 */
unsigned int NtsInterface::getExtensionFieldCount(void) const
{
	//get the actual extension field list out of the actual NTPPacket
	std::list<ntp::ExtensionField> extFieldList = ntpPacket.getExtensionFields();

	//return the amount of elements in the list.
	return extFieldList.size();
}

/**
 * @brief Method to get the ExtensionField type of a certain ExtensionField.
 * @details This method temporary stores the ExtensionField list and creates a iterator of the list.
 * 			This iterator will set on the position given by the index parameter. The type
 * 			of this ExtensionField element will readout an return to the calling method.\n\n
 * 			Note: The first ExtensionField in the list will readout with index 0.
 *
 * @param [in] index Integer. Position of the regarding ExtensionField element in the list.
 *
 * @return Returns an unsigned short value. It contains field type of the ExtensionField at the position <i>index</i> in the ExtensionField list.
 * 		   But if the given index is negative or higher than the highest index of the list, it returns zero. It also returns zero if there isn't
 * 		   any ExtensionField in the Message.
 */
unsigned short NtsInterface::getExtensionFieldType(unsigned int index) const
{
	//get the actual extension field list out of the actual NTPPacket
	std::list<ntp::ExtensionField>& extFieldList = ntpPacket.getExtensionFields();

	// validate parameters
	if (extFieldList.size() == 0 || index > (extFieldList.size() - 1))
	{
		// if the given index is top high or negative
		// or there is no extension field in this message
		// then return with return-value 0
		return 0;
	}

	//create an iterator on the list to select the first positioned element in the list
	std::list<ntp::ExtensionField>::iterator extFieldIterator = extFieldList.begin();

	// set the iterator at the given position in the list
	advance(extFieldIterator, index);

	//return the wanted field type of the selected element by the iterator
	return extFieldIterator->getFieldType();
}

/**
 * @brief Method to get the ExtensionField value (content) of a certain ExtensionField.
 * @details This method temporary stores the ExtensionField list and creates a iterator of the list.
 * 			This iterator will set on the position given by the index parameter. The value or the content
 * 			of this ExtensionField element will readout an return to the calling method.\n\n
 * 			Note: The first ExtensionField in the list will readout with index 0.
 *
 * @param [in] index Integer. Position of the regarding ExtensionField element in the list.
 *
 * @return Returns a vector of unsigned char. It contains the content (the value) of the ExtensionField object at the position <i>index</i>
 * 			in the ExtensionField list of the actual NTPPacket.
 * 			But if the given index is negative or higher than the highest index of the list, it returns a default (empty) char vector.
 * 			It also returns this if there isn't any ExtensionField in the Message.
 */
std::vector<unsigned char> NtsInterface::getExtensionFieldValue(unsigned int index) const
{
	//get the actual extension field list out of the actual NTPPacket
	std::list<ntp::ExtensionField>& extFieldList = ntpPacket.getExtensionFields();

	// validate index parameter
	if (extFieldList.size() == 0 || index > (extFieldList.size() - 1))
	{
		// if the given index is top high or negative
		// or there is no extension field in this message
		// then return with a default-value (empty vector)
		return std::vector<unsigned char>();
	}

	//create an iterator on the list to select a position in the list
	std::list<ntp::ExtensionField>::iterator extFieldIterator = extFieldList.begin();

	//set the iterator at the entry given by the parameter index
	advance(extFieldIterator, index);

	// return the Extension Field value at the position of the iterator
	return extFieldIterator->getValue();
}

/**
 * @brief Adds an element to the ExtensionField list in the actual NTPPacket.
 * @details This method get the relevant values of an ExtensionField object. It creates an new ExtensionField object
 * 			with these values an adds it to the ExtensionField list in the actual NTPPacket.
 *
 * @param [in] fieldType Unsigned short. FieldType value to select a type in this ExtensionField.
 * @param [in] value Vector of unsigned char. The content of this ExtensionField.
 *
 * @see
 * 		To get information about the given parameters:\n
 * 		ExtensionField.cpp\n
 * 		ExtensionField.hpp
 */
void NtsInterface::addExtensionField(unsigned short fieldType, std::vector<unsigned char> value)
{
	//create the ExtensionField to add
	ntp::ExtensionField extField = ntp::ExtensionField(fieldType, value);

	//give the ExtensionField to the NTPPacket to add it to the list
	ntpPacket.addExtensionField(extField);
}

/**
 * @brief Deletes an entry of the ExtensionField list in the actual NTPPacket.
 * @details This method gets the ExtensionField list from the actual NTPPacket. For this will it create an iterator
 * 			to select an object of the list. This iterator will be set on the position giving by the index value.
 * 			In the last step, the element at the iterators position will erase.\n\n
 * 			Note: The first ExtensionField in the list will erase with index 0.
 * 			If the given index is negative or higher than the highest index of the list, it returns. It also returns if there isn't
 * 		    any ExtensionField in the Message.
 *
 * @param [in] index Integer. Gives the position of the entry to erase out of the list.
 */
void NtsInterface::delExtensionField(unsigned int index)
{
	//get the actual extension field list out of the actual NTPPacket
	std::list<ntp::ExtensionField>& extFieldList = ntpPacket.getExtensionFields();

	// validate index parameter
	if (extFieldList.size() == 0 || index > (extFieldList.size() - 1))
	{
		// if the given index is top high or negative
		// or there is no extension field in this message
		// then return
		return;
	}

	//create an iterator on the list to select a position in the list
	std::list<ntp::ExtensionField>::iterator extFieldIterator = extFieldList.begin();

	//set the iterator at the entry to delete it
	advance(extFieldIterator, index);

	//erase the list entry at the iterators position
	extFieldList.erase(extFieldIterator);
}

/**
 * @brief Method to serialize the actual NTPPacket to build the MAC over this.
 * @details This method will create a vector of unsigned char elements. This will create by the NTPPacket
 * 			class. This is necessary to build a MAC over the data stream and add it to the send message.
 *
 * @return Returns a vector of unsigned char. It contains the serialized NTPPacket.
 */
std::vector<unsigned char> NtsInterface::getSerializedPackage(void) const
{
	//return the frame as a vector of unsigned char
	return ntpPacket.createFrame();
}

////////////////////////////////////////////////////////
/////////--Setter / Getter for flags--//////////////////
////////////////////////////////////////////////////////

/**
 * @brief Sets or resets the UseMessageForTimeSync flag.
 * @details The given value <i>state</i> will set or reset the flag named useMessageForTimeSync.
 * 			This flag blocks or allow the time synchronization with the actual Message.
 *
 * @param [in] state Boolean. Value to set the flag.
 */
void NtsInterface::setUseMessageForTimeSync(bool state)
{
	useMessageForTimeSync = state;
}

//getter for useMessageForTimeSync flag
bool NtsInterface::getUseMessageForTimeSync(void) const
{
	return useMessageForTimeSync;
}

/**
 * @brief Sets or Resets the sendResponseMessage flag.
 * @details The given value <i>state</i> will set or reset the flag named sendResponseMessage.
 * 			This flag is set, if a response message could be send. Then, the actual message object will send out.
 * 			If this flag is not set, no response message will send out.
 *
 * @param [in] state Boolean. Value to set the flag.
 */
void NtsInterface::setSendResponseMessage(bool state)
{
	sendResponseMessage = state;
}

//getter for sendResponseMessage flag
bool NtsInterface::getSendResponseMessage(void) const
{
	return sendResponseMessage;
}

/**
 * @brief Sets or Resets the generateResponseMessage flag.
 * @details The given value <i>state</i> will set or reset the flag named generateResponseMessage.
 * 			This flag is set, if NTS will send out a request immediately after the processing of the received Message object.
 * 			If it is true NTP generates with the NTS interface automatically a new response message.
 * 			If it is false NTP generates a response message at the next poll interval.
 *
 * @param [in] state Boolean. Value to set the flag.
 */
void NtsInterface::setGenerateResponseMessage(bool state)
{
	generateResponseMessage = state;
}

//getter for sendResponseMessage flag
bool NtsInterface::getGenerateResponseMessage(void) const
{
	return generateResponseMessage;
}
