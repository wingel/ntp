/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	Logger.hpp
 * @details Simple logging API
 * @date 	08.07.2016
 * @author 	silvan
 * @version	0.2
 */

#ifndef NTP_LOG_LOGGER_HPP_
#define NTP_LOG_LOGGER_HPP_

#include "LogFile.hpp"
#include <chrono>
#include <fstream>
#include <iostream>
#include <mutex> // std::mutex, std::lock_guard
#include <string>

// cut path off: __FILE__
using cstr = const char* const;
static constexpr cstr pastLastSlash(cstr str, cstr lastSlash)
{
	if (*str == '\0')
	{
		return lastSlash;
	}
	else if (*str == '\\' || *str == '/')
	{
		return pastLastSlash(str + 1, str + 1);
	}
	return pastLastSlash(str + 1, lastSlash);
}

static constexpr cstr pastLastSlash(cstr str)
{
	return pastLastSlash(str, str);
}

#define __FILENAME__ (pastLastSlash(__FILE__))

#ifdef __FUNCTION__
#define __THIS_FUNC__ __FUNCTION__
#else
// TODO: BESSER MACHEN
#define __THIS_FUNC__ __FUNCTION__
//#define __THIS_FUNC__ BOOST_CURRENT_FUNCTION
#endif

/// file used as logging output
#define LOG_FILE "log_ntp.txt"

/// see Logger::error() for description
#define LOG_ERROR_LVL 10

/// see Logger::info() for description
#define LOG_INFO_LVL 20

/// see Logger::warn() for description
#define LOG_WARN_LVL 30

/// see Logger::debug() for description
#define LOG_DEBUG_LVL 50

/// see Logger::chattyPollProcess() for description
#define LOG_CHATTY_POLLPROCESS (1 << 0)

/// see Logger::chattyUpdateSystemvariables() for description
#define LOG_CHATTY_UPDATESYSTEMVARIABLES (1 << 1)

/// helper macro
#define __STRINGIFY(STRING) #STRING

/// helper macro to transform __LINE__-macro into string
#define __TOSTRING(STRING) __STRINGIFY(STRING)

#define __LOG(SHORT, LOGGERFUNC, MESSAGE) LOGGERFUNC(SHORT "  " + ntp::Logger::padLocation(__FILENAME__, __THIS_FUNC__, __LINE__, 34) + MESSAGE)

/// logger instance
#define LOGGER ntp::Logger::getInstance()

/// log an error
#define LOG_ERROR(MESSAGE) __LOG("ERR", LOGGER.error, MESSAGE)

/// log a warning
#define WARN(MESSAGE) __LOG("WAR", LOGGER.warn, MESSAGE)

/// log an info
#define INFO(MESSAGE) __LOG("INF", LOGGER.info, MESSAGE)

/// log a debug information
#define DEBUG(MESSAGE) __LOG("DEB", LOGGER.debug, MESSAGE)

/// log chatty from poll process
#define CHATTY_POLL_PROCESS(MESSAGE) __LOG("POL", LOGGER.chattyPollProcess, MESSAGE)

/// log chatty when updating system variables
#define CHATTY_UPDATE_SYSTEM_VARIABLES(MESSAGE) __LOG("UPS", LOGGER.chattyUpdateSystemvariables, MESSAGE)

//  #####################################################
//  ####     Configuration    ###########################
//  #####################################################

/// loglevel to use (see Logger for detailed description)
#define LOG_USE_LVL LOG_DEBUG_LVL

/// chattyFlags to be used (see Logger for detailed description
#define LOG_USE_CHATTYFLAGS (LOG_CHATTY_POLLPROCESS | LOG_CHATTY_UPDATESYSTEMVARIABLES)

namespace ntp
{

/**
 * @brief	This is a simple class for logging information
 * 			when the program runs.
 * @details	This Logger supports four different logging levels and
 * 			two output streams (file and stdout).
 * 			The log level defines, which messages will be logged.
 * 			The higher the loglevel, the more messages will be logged.
 * 			So, if you use loglevel 30, messages from other levels from 1 to 29
 * 			will be logged as well.
 *
 * 			The desired loglevel is defined during compile-time using the
 * 			constant LOG_USE_LVL in Logger.hpp.
 */
class Logger
{

private:
	LogFile logfile;
	/// log level which will be used to filter logging output
	int loglevel;

	std::string filename;
	std::mutex m_mutex;

	bool enableConsoleOut;
	bool enableFileOut;

	/// flags which will enable certain logging functions
	unsigned char chattyFlags;
	/// the one and only instance of this class
	static Logger instance; //not used, because of static variable in getInstance()
	///Time when the program was started.
	static std::chrono::steady_clock::time_point startTimeLog;

	Logger(std::string filename, int loglevel, unsigned char chattyFlags);

	void output(const std::string& message);
	static double getProcessTime();

public:
	static Logger& getInstance();
	void info(const std::string& message);
	void debug(const std::string& message);
	void warn(const std::string& message);
	void error(const std::string& message);
	void chattyPollProcess(const std::string& message);
	void chattyUpdateSystemvariables(const std::string& message);

	void setLogLevel(int level);
	void setLogDir(std::string directory);
	void enableConsoleOutput(bool enable);
	void enableFileOutput(bool enable);

	static std::string padTo(const std::string& msg, unsigned int length);
	static std::string padLocation(const char* filename, const char* function, unsigned int line, unsigned int length);
	static int logLevelFromString(std::string loglevel);

	~Logger();
};

} // end of namespace ntp

#endif /* NTP_LOG_LOGGER_HPP_ */
