/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
* @file LogFile.hpp
* @author Christian Jütte
* @date 24.01.2017
*
* A class to log in files
*
*/

/** Kurzbeschreibung
* @param parameter paramtyp
* @return 
*
* Langbeschreibung
*/
#ifndef NTP_LOG_LOGFILE_HPP_
#define NTP_LOG_LOGFILE_HPP_

#include <fstream>
#include <mutex>
#include <string>

namespace ntp
{

/**
 * @brief	This is a simple class for logging information to files
 * 			when the program runs.
 * @details	To use this class, a LogFile simply needs to be constructed. With the function open,
 * 			a file can be opened for writing.
 * 			The file is closed on destruction.
 */
class LogFile
{
private:
	std::string m_filename; ///< Contains the address of the file
	std::fstream m_file;    ///< Stream to read and write to the file (mainly writing)
	std::mutex m_mutex;
	std::mutex m_mutexOpen;
	std::mutex m_mutexClose;

public:
	LogFile();
	/// The copy-constructor is explicitly deleted since we don't want to copy LogFiles
	LogFile(const LogFile&) = delete;
	~LogFile();

	bool open(std::string filename, bool overwrite);
	bool openAppend();
	bool close();
	bool isOpen();
	void appendLine(std::string str);
	void appendString(std::string str);
	void appendLineTime(std::string str);
};

} //end of namespace ntp

#endif /* NTP_LOG_LOGFILE_HPP_ */
