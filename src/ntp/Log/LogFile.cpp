/**
* @file LogFile.cpp
* @author Christian Jütte
* @date 24.01.2017
*
* Implementation of the LogFile class
*
*/

#include "LogFile.hpp"
#include "../Datatypes/Exceptions/FileException.hpp"
#include "../Utility/KernelInterface.hpp"
#include <chrono>
#include <iostream>

namespace ntp
{

/**
 * @brief  The Constructor of a LogFile.
 *
 */
LogFile::LogFile()
{
}

/**
 * @brief	Destructs the LogFile. This will close the file, if one is opened.
 *
 */

LogFile::~LogFile()
{
	if (m_file.is_open())
	{
		m_file.close();
	}
}

/**
 * @brief	Tries to open or create the file.
 * 			The flag overwrite specifies whether the file (if already present)
 * 			will be truncated or appended to.
 *
 * @param [in] filename The address and name of the file to be opened / created.
 *
 * @param [in] overwrite A flag to specify whether to overwrite an existing file or append to it.
 * 						A value of false means that the file should be left as it is, and new content should be appended to it.
 * 						A value of true means that the file should be truncated.
 *
 * @returns Returns true on success, false on failure. Please note: on failure, the other functions can still
 * 			be called, however no output will happen.
 *
 * @remarks This function currently does not support creating folders. Am implementation of this would be OS-specific (in the
 * 			KernelInterface) or would use other libraries.
 */

bool LogFile::open(std::string filename, bool overwrite)
{
	//std::lock_guard<std::mutex> lck(m_mutexOpen);
	bool success = true;

	std::ios_base::openmode mode = std::fstream::out;
	if (overwrite)
	{
		std::string file1;
		std::string file2;

		file1 = filename + ".old_" + std::to_string(9);
		std::remove(file1.c_str());

		for (int i = 8; i >= 1; i--)
		{
			file1 = filename + ".old_" + std::to_string(i);
			file2 = filename + ".old_" + std::to_string(i + 1);
			rename(file1.c_str(), file2.c_str());
		}

		file1 = filename + ".old_" + std::to_string(1);
		rename(filename.c_str(), file1.c_str());

		mode = mode | std::fstream::trunc;
	}
	else
	{
		mode = mode | std::fstream::app;
	}

	try
	{
		m_file.open(filename, mode);
		if (!m_file.is_open())
		{
			success = false;
			WARN("Could not open logging file " + filename + ". Maybe the directory does not exist?");
		}
	}
	catch (std::exception& ex)
	{
		success = false;
		WARN("Could not open logging file " + filename + "due to: " + ex.what());
	}

	m_filename = filename;

	return success;
}

bool LogFile::openAppend()
{
	std::lock_guard<std::mutex> lck(m_mutexOpen);
	if (m_filename.length() == 0)
	{
		return false;
	}

	try
	{
		m_file.open(m_filename, std::fstream::out | std::fstream::app);
		if (!m_file.is_open())
		{
			WARN("Could not open logging file " + m_filename + ".");
			return false;
		}
	}
	catch (std::exception& ex)
	{
		WARN("Could not open logging file " + m_filename + "due to: " + ex.what());
		return false;
	}

	return true;
}

/**
 * @brief Closes the file.
 *
 * @returns True on success, false on failure.
 */
bool LogFile::close()
{
	//std::lock_guard<std::mutex> lck(m_mutexClose);
	m_file.close();
	return !isOpen();
}

/**
 * @brief Returns true if the file is open, false if note.
 * 		  Please note: Appending to a file that is not open will not lead
 * 		  to any errors, it will simply not write to the file.
 *
 */
bool LogFile::isOpen()
{
	return m_file.is_open();
}

/**
 * @brief	Appends the supplied string and an additional linebreak ("\n") to the file.
 *
 */

void LogFile::appendLine(std::string str)
{
	std::lock_guard<std::mutex> lck(m_mutex);
	// allows log rotate by closing the file
	if (m_file.is_open() == false)
	{
		openAppend(); // TODO: fix this crap
	}

	m_file << str << "\n";

	close(); // TODO: fix this crap
}

/**
 * @brief	Appends the supplied string to the file.
 *
 */

void LogFile::appendString(std::string str)
{
	std::lock_guard<std::mutex> lck(m_mutex);
	// allows log rotate by closing the file
	if (m_file.is_open() == false)
	{
		openAppend(); // TODO: fix this crap
	}

	m_file << str;

	close(); // TODO: fix this crap
}

/**
 * @brief	Appends the supplied string to the file, preceded by the current
 * 			process time in seconds, separated by a tab ("\t").
 * 			Also appends a "\n" after the string.
 *
 */

void LogFile::appendLineTime(std::string str)
{
	std::lock_guard<std::mutex> lck(m_mutex);
	// allows log rotate by closing the file
	if (m_file.is_open() == false)
	{
		openAppend(); // TODO: fix this crap
	}

	m_file << str << "\n";

	close(); // TODO: fix this crap
}

} //end of namespace ntp
