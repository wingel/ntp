# build native image by default
# change with --build-arg ARCH=armhf
ARG ARCH=native

#native base
FROM debian:buster-slim as native

# armhf base
FROM arm32v7/debian:buster-slim as armhf
ADD qemu-arm-static.tar.gz /usr/bin

# common pre-build
FROM ${ARCH} as base
RUN apt update

# build stage
FROM base as builder

RUN apt install -y --no-install-recommends build-essential curl ninja-build cmake libssl-dev libboost-filesystem-dev libboost-system-dev libboost-log-dev libboost-regex-dev

WORKDIR /ntp/

COPY src/ /usr/src/ntp/src/
COPY nts/ /usr/src/ntp/nts/
COPY CMakeLists.txt /usr/src/ntp/

RUN cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DNTP_STATIC_BUILD=ON /usr/src/ntp
RUN cmake --build . --target ntp

# final image
FROM base

WORKDIR /ntp/

COPY --from=builder /ntp/src/ntp /usr/bin/
COPY ./bin .
RUN apt install -y --no-install-recommends libc6


CMD ["/usr/bin/ntp"]
